﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using HeraclitusMapLib.Layers;
using HeraclitusMapLib.Map;
using HeraclitusMapLib.Map.Google;
using HeraclitusMapLib.Framework;
using PixelFormat = System.Drawing.Imaging.PixelFormat;
using HeraclitusMapLib.Controls;

namespace HeraclitusMapLib.Forms
{
    public partial class FrmMapDownloader : Form
    {
        private readonly CancellationTokenSource _tokenSource;
        private Task _mapTask;

        protected delegate void ProgressEventDelegate(int progress, int mapBlockNumber, int mapBlockCount);

        protected event ProgressEventDelegate ProgressEvent;

        protected delegate void SaveMapEventDelegate(Bitmap image);

        protected event SaveMapEventDelegate SaveMapEvent;

        protected CoordinateRectangle _mapView;
        protected GoogleMapType _mapType;
        private string _mapCacheLocalPath;

        private enum WorkMode
        {
            Download,
            SaveToImageFile
        };

        private WorkMode _workMode;

        private PixelFormat _mapPiFormat;
        private int _mapLevel;

        public static void DownloadMap(GoogleMapType mapType, CoordinateRectangle mapView, string mapCacheLocalPath)
        {
            var frm = new FrmMapDownloader
                {
                    _workMode = WorkMode.Download,
                    Text = @"Download Map",
                    labelInfo = {Text = @"Cache all map on local disk"},
                };
            frm._mapView = mapView;
            frm._mapType = mapType;
            frm._mapCacheLocalPath = mapCacheLocalPath;
            frm.ShowDialog();
        }

        public static void SaveMapAsImage(GoogleMapType mapType, CoordinateRectangle mapView, PixelFormat mapPiFormat, int mapLevel, string mapCacheLocalPath)
        {
            var frm = new FrmMapDownloader
                {
                    _workMode = WorkMode.SaveToImageFile,
                    _mapPiFormat = mapPiFormat,
                    _mapLevel = mapLevel,
                    Text = @"Save Map As Image",
                    labelInfo = {Text = @"Save big map as one image"}
                };
            frm._mapView = mapView;
            frm._mapType = mapType;
            frm._mapCacheLocalPath = mapCacheLocalPath;
            frm.ShowDialog();
        }

        private FrmMapDownloader()
        {
            InitializeComponent();

            _tokenSource = new CancellationTokenSource();
            ProgressEvent += OnProgress;
            SaveMapEvent += OnSaveMap;
        }

        private void FrmMapDownloader_Load(object sender, EventArgs e)
        {
            var token = _tokenSource.Token;
            switch (_workMode)
            {
                case WorkMode.Download:
                    _mapTask = Task.Factory.StartNew(() => DownloadThread(token), token);
                    break;
                case WorkMode.SaveToImageFile:
                    _mapTask = Task.Factory.StartNew(() => GetFullMapThread(token), token);
                    break;
            }
        }

        private void FrmMapDownloader_FormClosing(object sender, FormClosingEventArgs e)
        {
            _tokenSource.Cancel();
            Task.WaitAll(_mapTask);
            e.Cancel = false;
        }

        protected void OnProgress(int progress, int mapBlockNumber, int mapBlockCount)
        {
            if (progress > 100)
            {
                Close();
                return;
            }
            if (mapBlockNumber % 10 == 0 || progress >= 100)
            {
                progressBar1.Value = progress;
                labProceed.Text = mapBlockNumber.ToString(CultureInfo.InvariantCulture);
                labTotal.Text = mapBlockCount.ToString(CultureInfo.InvariantCulture);
            }
        }

        protected void OnSaveMap(Bitmap image)
        {
            try
            {
                if (saveFileDialog1.ShowDialog(this) == DialogResult.OK)
                {
                    /*var palette = BitmapPalettes.Halftone256;

                    var idxImage = new RenderTargetBitmap(
                        image.Width,
                        image.Height,
                        Screen.PrimaryScreen.BitsPerPixel,
                        Screen.PrimaryScreen.BitsPerPixel,
                        PixelFormats.Indexed8);
                    var visual = new DrawingVisual();
                    var context = visual.RenderOpen();
                    context.DrawImage(image, );
                    idxImage.Render();
                    Graphics.FromImage(idxImage);             

                    */
                    {
                        image.Save(saveFileDialog1.FileName, ImageFormat.Png);
                    }
                }
            }
            finally
            {
                Close();
            }
        }

        private void DownloadThread(CancellationToken ct)
        {            
            var leftBound = new Coordinate(_mapView.Left, _mapView.Top);
            var rightBound = new Coordinate(_mapView.Right, _mapView.Bottom);

            var rectBound = new CoordinateRectangle(leftBound, rightBound);

            var mapBlockCount = 0;
            for (var mapLevel = MapCtl.MinZoomLevel; mapLevel <= MapCtl.MaxZoomLevel; mapLevel++)
            {
                var mapWidth = Convert.ToInt32((new GoogleCoordinate(rectBound.RightTop, mapLevel)).X - (new GoogleCoordinate(rectBound.LeftTop, mapLevel)).X) + 2 * GoogleBlock.BlockSize;
                var mapHeight = Convert.ToInt32((new GoogleCoordinate(rectBound.LeftBottom, mapLevel)).Y - (new GoogleCoordinate(rectBound.LeftTop, mapLevel)).Y) + 2 * GoogleBlock.BlockSize;

                var viewBound = rectBound.LineMiddlePoint.GetScreenViewFromCenter(mapWidth, mapHeight, mapLevel);
                var blockView = viewBound.BlockView;
                mapBlockCount += (blockView.Right - blockView.Left + 1) * (blockView.Bottom - blockView.Top + 1);
            }

            var mapBlockNumber = 0;
            BeginInvoke(ProgressEvent, new Object[] {mapBlockNumber * 100 / mapBlockCount, mapBlockNumber, mapBlockCount});

            for (var mapLevel = MapCtl.MinZoomLevel; mapLevel <= MapCtl.MaxZoomLevel; mapLevel++)
            {
                var mapWidth = Convert.ToInt32((new GoogleCoordinate(rectBound.RightTop, mapLevel)).X - (new GoogleCoordinate(rectBound.LeftTop, mapLevel)).X) + 2 * GoogleBlock.BlockSize;
                var mapHeight = Convert.ToInt32((new GoogleCoordinate(rectBound.LeftBottom, mapLevel)).Y - (new GoogleCoordinate(rectBound.LeftTop, mapLevel)).Y) + 2 * GoogleBlock.BlockSize;

                var viewBound = rectBound.LineMiddlePoint.GetScreenViewFromCenter(mapWidth, mapHeight, mapLevel);
                var blockView = viewBound.BlockView;

                for (var x = blockView.Left; x <= blockView.Right; x++)
                {
                    for (var y = blockView.Top; y <= blockView.Bottom; y++)
                    {
                        var block = new GoogleBlock(x, y, mapLevel);

                        var fileName = MapLayer.GetCahedMapFileName(this._mapCacheLocalPath, this._mapType, block);
                        if (!File.Exists(fileName))
                            MapLayer.DownloadImageFromGoogle(this._mapCacheLocalPath, _mapType, block, false);

                        mapBlockNumber++;

                        BeginInvoke(ProgressEvent, new Object[] {mapBlockNumber * 100 / mapBlockCount, mapBlockNumber, mapBlockCount});

                        if (ct.IsCancellationRequested)
                            return;
                    }
                }
            }
            BeginInvoke(ProgressEvent, new Object[] {101, mapBlockNumber, mapBlockCount});
        }

        private void GetFullMapThread(CancellationToken ct)
        {
            var leftBound = new Coordinate(_mapView.Left, _mapView.Top);
            var rightBound = new Coordinate(_mapView.Right, _mapView.Bottom);

            var rectBound = new CoordinateRectangle(leftBound, rightBound);

            try
            {
                var mapWidth = Convert.ToInt32((new GoogleCoordinate(rectBound.RightTop, _mapLevel)).X - (new GoogleCoordinate(rectBound.LeftTop, _mapLevel)).X) + 2 * GoogleBlock.BlockSize;
                var mapHeight = Convert.ToInt32((new GoogleCoordinate(rectBound.LeftBottom, _mapLevel)).Y - (new GoogleCoordinate(rectBound.LeftTop, _mapLevel)).Y) + 2 * GoogleBlock.BlockSize;

                var image = GraphicLayer.CreateCompatibleBitmap(null, mapWidth, mapHeight, _mapPiFormat);
                var graphics = Graphics.FromImage(image);

                var viewBound = rectBound.LineMiddlePoint.GetScreenViewFromCenter(mapWidth, mapHeight, _mapLevel);
                var blockView = viewBound.BlockView;
                var mapBlockCount = (blockView.Right - blockView.Left + 1) * (blockView.Bottom - blockView.Top + 1);
                var mapBlockNumber = 0;
            
                BeginInvoke(ProgressEvent, new Object[] {mapBlockNumber * 100 / mapBlockCount, mapBlockNumber, mapBlockCount});

                for (var x = blockView.Left; x <= blockView.Right; x++)
                {
                    for (var y = blockView.Top; y <= blockView.Bottom; y++)
                    {
                        var block = new GoogleBlock(x, y, _mapLevel);
                        var bmp = GraphicLayer.CreateCompatibleBitmap(
                            MapLayer.DownloadImageFromFile(_mapCacheLocalPath, _mapType, block) ?? MapLayer.DownloadImageFromGoogle(_mapCacheLocalPath, _mapType, block, true),
                            GoogleBlock.BlockSize, GoogleBlock.BlockSize, _mapPiFormat);

                        var rect = ((GoogleRectangle) block).GetScreenRect(viewBound);
                        graphics.DrawImageUnscaled(bmp, rect.Location.X, rect.Location.Y);

                        mapBlockNumber++;

                        BeginInvoke(ProgressEvent, new Object[]{mapBlockNumber * 100 / mapBlockCount, mapBlockNumber, mapBlockCount});

                        if (ct.IsCancellationRequested)
                            return;
                    }
                }

                BeginInvoke(SaveMapEvent, new Object[] {image});
            }
            catch (Exception e)
            {
                BeginInvoke(ProgressEvent, new Object[] { 101, 0, 0 });
                MessageBox.Show(e.Message, @"Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
