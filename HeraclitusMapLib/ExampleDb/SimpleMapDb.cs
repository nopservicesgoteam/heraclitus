﻿using System.Data;
using System.Windows.Forms;

namespace HeraclitusMapLib.ExampleDb
{

    public partial class SimpleMapDb
    {

        public static BindingSource CreateDataSource(DataTable table)
        {
            var bindingSource = new BindingSource { DataSource = table.DataSet, DataMember = table.TableName };

            return bindingSource;
        }
    }
}
