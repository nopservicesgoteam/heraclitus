using HeraclitusMapLib.Framework;
using HeraclitusMapLib.Map.Google;
using System;
using System.Linq;
using System.Windows.Forms;

namespace HeraclitusMapLib.Controls
{
    public partial class MapSimpleButtonPanel : UserControl
    {
        public MapSimpleButtonPanel()
        {
            InitializeComponent();
            this.populateMapTypesFromEnum(typeof(GoogleMapType));
        }

        public int Level
        {
            get
            {
                return zoomLevel.Value;
            }
            set
            {
                zoomLevel.Value = value;
            }
        }

        public int MapTypeIndex
        {
            get
            {
                return cmbMapType.SelectedIndex;
            }
            set
            {
                try
                {
                    cmbMapType.SelectedIndex = value;
                }
                catch (Exception)
                {
                }
                
            }
        }

        public void populateMapTypesFromEnum(Type type)
        {
            _MapTypeIndexChangedEnabled = false;
            cmbMapType.DataSource = Enum.GetValues(type);
            _MapTypeIndexChangedEnabled = true;
        }

        public class LevelValueArgs : EventArgs
        {
            public int Level { get; private set; }

            public LevelValueArgs(int level)
            {
                Level = level;
            }
        }
        public event EventHandler<LevelValueArgs> LevelValueChanged;


        public class MapTypeIndexArgs : EventArgs
        {
            public int MapTypeIndex { get; private set; }

            public MapTypeIndexArgs(int mapTypeIndex)
            {
                MapTypeIndex = mapTypeIndex;
            }
        }
        private bool _MapTypeIndexChangedEnabled = true;
        public event EventHandler<MapTypeIndexArgs> MapTypeIndexChanged;

        public event EventHandler CenterMapClicked;

        public event EventHandler PrintMapClicked;        

        public event EventHandler SaveAllMapClicked;

        public event EventHandler SaveMapAsImageClicked;

        private void FrmDesignPanel_Load(object sender, EventArgs e)
        {
            zoomLevel.Maximum = MapCtl.MaxZoomLevel;
            zoomLevel.Minimum = MapCtl.MinZoomLevel;
            zoomLevel.Value = GraphicLayer.DefaultStartZoomLevel;
        }

        private void zoomLevel_ValueChanged(object sender, EventArgs e)
        {
            if (LevelValueChanged != null && zoomLevel.Value >= MapCtl.MinZoomLevel
                && zoomLevel.Value <= MapCtl.MaxZoomLevel)
            {
                LevelValueChanged(this, new LevelValueArgs(zoomLevel.Value));
            }
        }

        private void cmbMapType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (MapTypeIndexChanged != null && _MapTypeIndexChangedEnabled)
            {
                MapTypeIndexChanged(this, new MapTypeIndexArgs(MapTypeIndex));
            }
        }

        private void btnCenterMap_Click(object sender, EventArgs e)
        {
            CenterMapClicked?.Invoke(this, e);
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            PrintMapClicked?.Invoke(this, EventArgs.Empty);
        }

        private void btnCacheAllMap_Click(object sender, EventArgs  e)
        {
            SaveAllMapClicked?.Invoke(this, EventArgs.Empty);
        }

        private void btnSaveMapAsImage_Click(object sender, EventArgs e)
        {
            SaveMapAsImageClicked?.Invoke(this, EventArgs.Empty);
        }


    }
}