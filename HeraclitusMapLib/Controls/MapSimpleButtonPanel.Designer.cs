namespace HeraclitusMapLib.Controls
{
    partial class MapSimpleButtonPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.zoomLevel = new System.Windows.Forms.TrackBar();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btnSaveMapAsImage = new System.Windows.Forms.Button();
            this.btnCacheAllMap = new System.Windows.Forms.Button();
            this.btnCenterMap = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.cmbMapType = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.zoomLevel)).BeginInit();
            this.SuspendLayout();
            // 
            // zoomLevel
            // 
            this.zoomLevel.LargeChange = 2;
            this.zoomLevel.Location = new System.Drawing.Point(3, 3);
            this.zoomLevel.Maximum = 20;
            this.zoomLevel.Minimum = 10;
            this.zoomLevel.Name = "zoomLevel";
            this.zoomLevel.Size = new System.Drawing.Size(126, 45);
            this.zoomLevel.TabIndex = 10;
            this.toolTip1.SetToolTip(this.zoomLevel, "Change map zoom level");
            this.zoomLevel.Value = 12;
            this.zoomLevel.ValueChanged += new System.EventHandler(this.zoomLevel_ValueChanged);
            // 
            // btnSaveMapAsImage
            // 
            this.btnSaveMapAsImage.Image = global::HeraclitusMapLib.Properties.Resources.save_2_16x16;
            this.btnSaveMapAsImage.Location = new System.Drawing.Point(323, 3);
            this.btnSaveMapAsImage.Name = "btnSaveMapAsImage";
            this.btnSaveMapAsImage.Size = new System.Drawing.Size(33, 32);
            this.btnSaveMapAsImage.TabIndex = 13;
            this.toolTip1.SetToolTip(this.btnSaveMapAsImage, "Save map view as image to file");
            this.btnSaveMapAsImage.UseVisualStyleBackColor = true;
            this.btnSaveMapAsImage.Click += new System.EventHandler(this.btnSaveMapAsImage_Click);
            // 
            // btnCacheAllMap
            // 
            this.btnCacheAllMap.Image = global::HeraclitusMapLib.Properties.Resources.redis_cache;
            this.btnCacheAllMap.Location = new System.Drawing.Point(284, 3);
            this.btnCacheAllMap.Name = "btnCacheAllMap";
            this.btnCacheAllMap.Size = new System.Drawing.Size(33, 32);
            this.btnCacheAllMap.TabIndex = 12;
            this.toolTip1.SetToolTip(this.btnCacheAllMap, "Cache current map view (all levels) to local disk");
            this.btnCacheAllMap.UseVisualStyleBackColor = true;
            this.btnCacheAllMap.Click += new System.EventHandler(this.btnCacheAllMap_Click);
            // 
            // btnCenterMap
            // 
            this.btnCenterMap.Image = global::HeraclitusMapLib.Properties.Resources.centermap_24;
            this.btnCenterMap.Location = new System.Drawing.Point(401, 3);
            this.btnCenterMap.Name = "btnCenterMap";
            this.btnCenterMap.Size = new System.Drawing.Size(33, 32);
            this.btnCenterMap.TabIndex = 11;
            this.toolTip1.SetToolTip(this.btnCenterMap, "Center map");
            this.btnCenterMap.UseVisualStyleBackColor = true;
            this.btnCenterMap.Click += new System.EventHandler(this.btnCenterMap_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Image = global::HeraclitusMapLib.Properties.Resources.print_24;
            this.btnPrint.Location = new System.Drawing.Point(362, 3);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(33, 32);
            this.btnPrint.TabIndex = 9;
            this.toolTip1.SetToolTip(this.btnPrint, "Print current map view");
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // cmbMapType
            // 
            this.cmbMapType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMapType.FormattingEnabled = true;
            this.cmbMapType.Location = new System.Drawing.Point(135, 8);
            this.cmbMapType.Name = "cmbMapType";
            this.cmbMapType.Size = new System.Drawing.Size(143, 21);
            this.cmbMapType.TabIndex = 16;
            this.cmbMapType.SelectedIndexChanged += new System.EventHandler(this.cmbMapType_SelectedIndexChanged);
            // 
            // MapSimpleButtonPanel
            // 
            this.Controls.Add(this.cmbMapType);
            this.Controls.Add(this.btnSaveMapAsImage);
            this.Controls.Add(this.btnCacheAllMap);
            this.Controls.Add(this.btnCenterMap);
            this.Controls.Add(this.zoomLevel);
            this.Controls.Add(this.btnPrint);
            this.MaximumSize = new System.Drawing.Size(20000, 38);
            this.MinimumSize = new System.Drawing.Size(325, 38);
            this.Name = "MapSimpleButtonPanel";
            this.Size = new System.Drawing.Size(442, 38);
            this.Load += new System.EventHandler(this.FrmDesignPanel_Load);
            ((System.ComponentModel.ISupportInitialize)(this.zoomLevel)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnCenterMap;
        private System.Windows.Forms.TrackBar zoomLevel;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnCacheAllMap;
        private System.Windows.Forms.Button btnSaveMapAsImage;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ComboBox cmbMapType;
    }
}
