using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Threading.Tasks;
using System.Windows.Forms;
using HeraclitusMapLib.ExampleDb;
using HeraclitusMapLib.Framework;
using HeraclitusMapLib.Framework.WorkerThread.Types;
using HeraclitusMapLib.Layers;
using HeraclitusMapLib.Map;
using HeraclitusMapLib.Map.Google;

using HashItem = System.Collections.Generic.KeyValuePair<double, int>;

namespace HeraclitusMapLib.Controls
{
    public partial class MapCtl : UserControl
    {
        public static int MinZoomLevel = 5;
        public static int MaxZoomLevel = 19;
        public readonly static Coordinate DefaultCenter = new Coordinate(23.72784, 37.98244);

        private readonly MapLayer _mapLayer;
        private readonly NetLayer _netLayer;
        private readonly FlightLayer _flightLayer;

        GoogleMapType _mapType = GoogleMapType.StandardRoadmap;
        public GoogleMapType MapType { 
            get => _mapType;
            set
            {
                _mapType = value;
                if (_mapLayer != null)
                {
                    _mapLayer.MapType = _mapType;
                }
                if (MapTypeIndexChanged!= null)
                    MapTypeIndexChanged(this, new ButtonPanelCtl.MapTypeIndexArgs((int)value));
                RefreshControl(true);
            }
        }

        private string _mapCacheLocalPath = null;
        public string MapCacheLocalPath
        {
            get => _mapCacheLocalPath;
            set
            {
                _mapCacheLocalPath = value;
                if (_mapLayer != null)
                {
                    _mapLayer.MapCacheLocalPath = _mapCacheLocalPath;
                }
                RefreshControl(true);
            }
        }


        public int Level
        {
            get
            {
                return _mapLayer.Level;
            }
            set
            {
                if (value != _mapLayer.Level)
                {
                    _mapLayer.Level = value;
                    _netLayer.Level = value;
                    _flightLayer.Level = value;
                    if (LevelValueChanged != null)
                        LevelValueChanged(this, new ButtonPanelCtl.LevelValueArgs(value));
                }
            }
        }
        public event EventHandler<ButtonPanelCtl.LevelValueArgs> LevelValueChanged;
        public event EventHandler<ButtonPanelCtl.MapTypeIndexArgs> MapTypeIndexChanged;

        public PixelFormat PiFormat
        {
            get { return _mapLayer.PiFormat; }    
        }

        public Coordinate CenterCoordinate
        {
            get
            {
                return _mapLayer.CenterCoordinate;
            }
            set
            {
                _mapLayer.CenterCoordinate = value;
                _netLayer.CenterCoordinate = value;
                _flightLayer.CenterCoordinate = value;
            }
        }

        public GoogleRectangle ScreenView
        {
            get
            {
                return _mapLayer.ScreenView;
            }
        }

        public CoordinateRectangle CoordinateView
        {
            get
            {
                return _mapLayer.CoordinateView;
            }
        }


        private bool _mouseMoveMap;
        private Point _mousePreviousLocation;
        private Coordinate _coordinatePreviosLocation;
        private bool ShiftKey { get; set; }
        

        //protected override CreateParams CreateParams
        //{
        //    get
        //    {
        //        var handleParam = base.CreateParams;
        //        handleParam.ExStyle |= 0x02000000;   // WS_EX_COMPOSITED       
        //        return handleParam;
        //    }
        //}

        public MapCtl()
        {
            InitializeComponent();

            var center = DefaultCenter;

            _mapLayer = new MapLayer(Width, Height, center, GraphicLayer.DefaultStartZoomLevel, this.MapType, this.MapCacheLocalPath);
            _netLayer = new NetLayer(Width, Height, center, GraphicLayer.DefaultStartZoomLevel);
            _flightLayer = new FlightLayer(Width, Height, center, GraphicLayer.DefaultStartZoomLevel);
            _mapLayer.DrawLayerBuffer += Layer_DrawBufferChanged;
            _netLayer.DrawLayerBuffer += Layer_DrawBufferChanged;
            _flightLayer.DrawLayerBuffer += Layer_DrawBufferChanged;

            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            SetStyle(ControlStyles.UserPaint, true);
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            SetStyle(ControlStyles.Opaque, true);
            SetStyle(ControlStyles.SupportsTransparentBackColor, false);
            
            MouseWheel += MapCtl_MouseWheel;

            //GenerateSampleData();

            RefreshControl();
        }

        private void MapCtl_Resize(object sender, EventArgs e)
        {
            if (_mapLayer.Terminating) return;

            _mapLayer.Resize(Width, Height);
            _netLayer.Resize(Width, Height);
            _flightLayer.Resize(Width, Height);
        }

        private void DrawLayers(Graphics clientDC, Rectangle clipRectangle)
        {
            if (_mapLayer.Terminating) return;

            //clientDC.Clear(Color.White);
            
            _mapLayer.DrawBufferToScreen(clientDC, clipRectangle);
            _netLayer.DrawBufferToScreen(clientDC, clipRectangle);
            _flightLayer.DrawBufferToScreen(clientDC, clipRectangle);
        }

        public void RefreshControl(bool forceFetchImages = false)
        {
            if (forceFetchImages)
            {
                _mapLayer.InvalidateLayer();                
            }
            _mapLayer.Update(new Rectangle(0, 0, Width, Height));
            
            //_netLayer.ReloadData();
            //
        }

        public void ClearFlightData()
        {
            this._flightLayer.ClearData();
        }

        public void SetFlightData(List<Coordinate> data)
        {
            this._flightLayer.SetData(data);
        }

        public void SetFlightPointIndex(int newPointIndex)
        {
            this._flightLayer.SetPointIndex(newPointIndex);
        }

        private void GenerateSampleData()
        {

            //--sample for show smothness
            var rnd = new Random();
            var rangeX = 100;
            var rangeY = 100;

            var longitude1 = (Decimal)DefaultCenter.Longitude;
            var latitude1 = (Decimal)DefaultCenter.Latitude;

            var cableDbRows = new SimpleMapDb.CablesDataTable();
            var vertexDbRows = new SimpleMapDb.VertexesDataTable();
            while (cableDbRows.Count < 50)
            {
                var cableRow = cableDbRows.NewCablesRow();

                cableRow.Longitude1 = longitude1;
                cableRow.Latitude1 = latitude1;
                //cableRow.Longitude2 = Convert.ToDecimal(Settings.Default.LeftMapBound + (double)rnd.Next(0, rangeX) / 100000);
                //cableRow.Latitude2 = Convert.ToDecimal(Settings.Default.BottomMapBound + (double)rnd.Next(0, rangeY) / 100000);
                cableRow.Longitude2 = cableRow.Longitude1 + (decimal)0.001;
                cableRow.Latitude2 = cableRow.Latitude1 + (decimal)0.001;
                var rect = new CoordinateRectangle(cableRow.Longitude1, cableRow.Latitude1, cableRow.Longitude2, cableRow.Latitude2);
                cableRow.Length = Convert.ToDecimal(rect.LineLength);
                if (cableRow.Length <= 5000 && cableRow.Length > 10)
                {
                    longitude1 = cableRow.Longitude2;
                    latitude1 = cableRow.Latitude2;
                    cableRow.Caption = rect.ToString();
                    cableDbRows.AddCablesRow(cableRow);

                    var vertexRow = vertexDbRows.NewVertexesRow();

                    vertexRow.Longitude = longitude1;
                    vertexRow.Latitude = latitude1;

                    var pt = new Coordinate(vertexRow.Longitude, vertexRow.Latitude);
                    vertexRow.Caption = pt.ToString();
                    if (cableDbRows.Count == 28)
                    {
                        vertexDbRows.AddVertexesRow(vertexRow);
                    }
                }
            }
            SetOverlayData(cableDbRows, vertexDbRows);
            //--end sample
        }

        public void SetOverlayData(SimpleMapDb.CablesDataTable cables, SimpleMapDb.VertexesDataTable vertexes)
        {
            _netLayer.ClearData();
            if (cables != null || vertexes != null) {
                if (vertexes != null)
                {
                    _netLayer.MergeData(vertexes);
                }                
                if (cables != null)
                {
                    _netLayer.MergeData(cables);
                }                
                this.CenterCoordinate = DefaultCenter;
            }
        }

        private void MapCtl_Paint(object sender, PaintEventArgs e)
        {
            if (_mapLayer.Terminating) return;

            var g = e.Graphics;
            
            g.InterpolationMode = InterpolationMode.Low;
            g.CompositingQuality = CompositingQuality.HighSpeed;
            g.SmoothingMode = SmoothingMode.HighSpeed;

            DrawLayers(g, e.ClipRectangle);
        }

        public Bitmap GetMapImageForPrint(int mapWidth, int mapHeight)
        {
            var image = GraphicLayer.CreateCompatibleBitmap(null, mapWidth, mapHeight, PiFormat);
            var g = Graphics.FromImage(image);
            var r = new Rectangle(new Point(0, 0), image.Size);
            DrawLayers(g, r);
            return image;
        }

        public Bitmap GetMapImageForPrint()
        {
            return GetMapImageForPrint(Width, Height);
        }

        private void Layer_DrawBufferChanged(object sender, GraphicLayer.InvalidateLayerEventArgs e)
        {
            if (_mapLayer.Terminating) return;
            if (e == MainThreadEventArgs.Empty)
                Invalidate();

            else
                Invalidate(e.ClipRectangle);
        }

        public void ControlClosing()
        {
            _mapLayer.Terminating = true;
            _netLayer.Terminating = true;
            _flightLayer.Terminating = true;

            _mouseMoveMap = false;
        }

        public void SetCenterMap()
        {
            if (_mapLayer.Terminating) return;

            CenterCoordinate = DefaultCenter;
        }

        public void MoveCenterMapObject(decimal longitude, decimal latitude)
        {
            if (_mapLayer.Terminating) return;

            CenterCoordinate = new Coordinate(longitude, latitude);
        }

        private bool FindMapObjects(Point location, out HashItem[] vertexRows, out HashItem[] cableRows)
        {
            HashItem[] vertexR = null;
            HashItem[] cableR = null;
            try
            {
                var coordinate = Coordinate.GetCoordinateFromScreen(_netLayer.ScreenView, location);
                var tasks = new List<Task>
                    {
                        Task.Factory.StartNew(() => vertexR = _netLayer.FindNearestVertex(coordinate)),
                        Task.Factory.StartNew(() => cableR = _netLayer.FindNearestCable(coordinate))
                    };

                Task.WaitAll(tasks.ToArray());
            }
            catch (Exception)
            {
                vertexR = null;
                cableR = null;
            }
            vertexRows = vertexR;
            cableRows = cableR;

            return (vertexRows != null && vertexRows.Length > 0) || (cableRows != null && cableRows.Length > 0);
        }

        private void MapCtl_MouseDown(object sender, MouseEventArgs e)
        {
            if (_mapLayer.Terminating) return;

            if (_mouseMoveMap) return;

            if (e.Button == MouseButtons.Left)
            {

                HashItem[] vertexRows;
                HashItem[] cableRows;

                if (!FindMapObjects(e.Location, out vertexRows, out cableRows))
                {
                    _mousePreviousLocation = new Point(e.X, e.Y);

                    if (ShiftKey)
                    {
                        MapCtl_MouseWheel(this, new MouseEventArgs(e.Button, e.Clicks, e.X, e.Y, 1));
                        CenterCoordinate = Coordinate.GetCoordinateFromScreen(_netLayer.ScreenView, _mousePreviousLocation);
                    }
                    else
                    {
                        _mouseMoveMap = true;
                        _coordinatePreviosLocation = CenterCoordinate;
                    }
                }
                else
                {
                    if (vertexRows.Length > 0)
                    {
                        var vertex = _netLayer.GetVertex(vertexRows[0].Value);
                        if (vertex != null) MessageBox.Show(vertex.Caption, @"Vertex found!");
                    }
                    else if (cableRows.Length > 0)
                    {
                        var cable = _netLayer.GetCable(cableRows[0].Value);
                        if (cable != null) MessageBox.Show(cable.Caption, @"Cable found!");
                    }
                }
            }
            else if (e.Button == MouseButtons.Right)
            {
                HashItem[] vertexRows;
                HashItem[] cableRows;

                if (!FindMapObjects(e.Location, out vertexRows, out cableRows))
                {
                    _mousePreviousLocation = new Point(e.X, e.Y);

                    if (ShiftKey)
                    {
                        MapCtl_MouseWheel(this, new MouseEventArgs(e.Button, e.Clicks, e.X, e.Y, -1));
                        CenterCoordinate = Coordinate.GetCoordinateFromScreen(_netLayer.ScreenView, _mousePreviousLocation);
                    }
                }
                else
                {
                    var menu = new ContextMenu();
                    
                    foreach(var row in vertexRows)
                    {
                        var vertex = _netLayer.GetVertex(row.Value);
                        var item = menu.MenuItems.Add(String.Format(@"Delete Vertex: {0}", vertex.Caption), MapCtl_DeleteVertexClick);
                        item.Tag = row.Value;
                    }
                    foreach (var row in cableRows)
                    {
                        var cable = _netLayer.GetCable(row.Value);
                        var item = menu.MenuItems.Add(String.Format(@"Delete Cable: {0}", cable.Caption), MapCtl_DeleteCableClick);
                        item.Tag = row.Value;
                    }
                    
                    menu.Show(this, e.Location);
                }
            }
        }

        private void MapCtl_DeleteVertexClick(object sender, EventArgs e)
        {
            var item = sender as MenuItem;
            if (item != null && item.Tag is int)
            {
                var menuObjectId = (int)item.Tag;
                _netLayer.RemoveVertex(menuObjectId);
            }
        }

        private void MapCtl_DeleteCableClick(object sender, EventArgs e)
        {
            var item = sender as MenuItem;
            if (item != null && item.Tag is int)
            {
                var menuObjectId = (int)item.Tag;
                _netLayer.RemoveCable(menuObjectId);
            }
        }

        private void MapCtl_DoubleClick(object sender, EventArgs e)
        {
            if (_mapLayer.Terminating) return;

            HashItem[] vertexRows;
            HashItem[] cableRows;

            if (!ShiftKey && !FindMapObjects(_mousePreviousLocation, out vertexRows, out cableRows))
            {
                CenterCoordinate = Coordinate.GetCoordinateFromScreen(_netLayer.ScreenView, _mousePreviousLocation);
            }
        }

        private void MapCtl_MouseUp(object sender, MouseEventArgs e)
        {
            _mouseMoveMap = false;
            _mousePreviousLocation = new Point(e.X, e.Y);
        }

        private void MapCtl_MouseWheel(object sender, MouseEventArgs e)
        {
            if (e.Delta != 0)
            {
                var newZoom = Level;
                newZoom += (e.Delta > 0) ? 1 : -1;
                if (newZoom < MapCtl.MinZoomLevel)
                    newZoom = MapCtl.MinZoomLevel;
                else if (newZoom > MapCtl.MaxZoomLevel)
                    newZoom = MapCtl.MaxZoomLevel;
                Level = newZoom;
            }
        }

        private void MapCtl_MouseMove(object sender, MouseEventArgs e)
        {
            if (_mapLayer.Terminating) return;

            if (_mouseMoveMap)
            {
                var deltaX = _mousePreviousLocation.X - e.X;
                var deltaY = _mousePreviousLocation.Y - e.Y;
                
                CenterCoordinate = _coordinatePreviosLocation + new GoogleCoordinate(deltaX, deltaY, Level);
            }
            else
            {
                if (String.IsNullOrEmpty(toolTip1.GetToolTip(this)))
                {
                    HashItem[] vertexRows;
                    HashItem[] cableRows;
                    FindMapObjects(e.Location, out vertexRows, out cableRows);

                    if (vertexRows.Length > 0)
                    {
                        var vertex = _netLayer.GetVertex(vertexRows[0].Value);

                        if (vertex != null)
                        {
                            toolTip1.RemoveAll();
                            toolTip1.Show(vertex.Caption, this, e.Location, 3000);
                        }
                    }
                    else if (cableRows.Length > 0)
                    {
                        var cable = _netLayer.GetCable(cableRows[0].Value);
                        if (cable != null)
                        {
                            toolTip1.RemoveAll();
                            toolTip1.Show(cable.Caption, this, e.Location, 3000);
                        }
                    }
                }
            }
        }

        private void MapCtl_KeyDown(object sender, KeyEventArgs e)
        {
            ShiftKey = e.KeyCode == Keys.ShiftKey;
        }

        private void MapCtl_KeyUp(object sender, KeyEventArgs e)
        {
            ShiftKey = false;
        }
    }
}
