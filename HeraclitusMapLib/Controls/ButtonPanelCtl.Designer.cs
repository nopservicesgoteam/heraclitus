namespace HeraclitusMapLib.Controls
{
    partial class ButtonPanelCtl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.labelControl1 = new System.Windows.Forms.Label();
            this.zoomLevel = new System.Windows.Forms.TrackBar();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btnSaveMapAsImage = new System.Windows.Forms.Button();
            this.btnCacheAllMap = new System.Windows.Forms.Button();
            this.btnCenterMap = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnRefreshMap = new System.Windows.Forms.Button();
            this.cmbMapType = new System.Windows.Forms.ComboBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.zoomLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(84, 13);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(40, 13);
            this.labelControl1.TabIndex = 3;
            this.labelControl1.Text = "Zoom:";
            // 
            // zoomLevel
            // 
            this.zoomLevel.LargeChange = 2;
            this.zoomLevel.Location = new System.Drawing.Point(130, 3);
            this.zoomLevel.Maximum = 20;
            this.zoomLevel.Minimum = 10;
            this.zoomLevel.Name = "zoomLevel";
            this.zoomLevel.Size = new System.Drawing.Size(114, 45);
            this.zoomLevel.TabIndex = 10;
            this.toolTip1.SetToolTip(this.zoomLevel, "Change map zoom level");
            this.zoomLevel.Value = 12;
            this.zoomLevel.ValueChanged += new System.EventHandler(this.zoomLevel_ValueChanged);
            // 
            // btnSaveMapAsImage
            // 
            this.btnSaveMapAsImage.Image = global::HeraclitusMapLib.Properties.Resources.save_2_16x16;
            this.btnSaveMapAsImage.Location = new System.Drawing.Point(522, 0);
            this.btnSaveMapAsImage.Name = "btnSaveMapAsImage";
            this.btnSaveMapAsImage.Size = new System.Drawing.Size(33, 32);
            this.btnSaveMapAsImage.TabIndex = 13;
            this.toolTip1.SetToolTip(this.btnSaveMapAsImage, "Save map view as image to file");
            this.btnSaveMapAsImage.UseVisualStyleBackColor = true;
            this.btnSaveMapAsImage.Click += new System.EventHandler(this.btnSaveMapAsImage_Click);
            // 
            // btnCacheAllMap
            // 
            this.btnCacheAllMap.Image = global::HeraclitusMapLib.Properties.Resources.redis_cache;
            this.btnCacheAllMap.Location = new System.Drawing.Point(483, 0);
            this.btnCacheAllMap.Name = "btnCacheAllMap";
            this.btnCacheAllMap.Size = new System.Drawing.Size(33, 32);
            this.btnCacheAllMap.TabIndex = 12;
            this.toolTip1.SetToolTip(this.btnCacheAllMap, "Cache current map view (all levels) to local disk");
            this.btnCacheAllMap.UseVisualStyleBackColor = true;
            this.btnCacheAllMap.Click += new System.EventHandler(this.btnCacheAllMap_Click);
            // 
            // btnCenterMap
            // 
            this.btnCenterMap.Image = global::HeraclitusMapLib.Properties.Resources.centermap_24;
            this.btnCenterMap.Location = new System.Drawing.Point(444, 0);
            this.btnCenterMap.Name = "btnCenterMap";
            this.btnCenterMap.Size = new System.Drawing.Size(33, 32);
            this.btnCenterMap.TabIndex = 11;
            this.toolTip1.SetToolTip(this.btnCenterMap, "Center map");
            this.btnCenterMap.UseVisualStyleBackColor = true;
            this.btnCenterMap.Click += new System.EventHandler(this.btnCenterMap_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Image = global::HeraclitusMapLib.Properties.Resources.print_24;
            this.btnPrint.Location = new System.Drawing.Point(42, 3);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(33, 32);
            this.btnPrint.TabIndex = 9;
            this.toolTip1.SetToolTip(this.btnPrint, "Print current map view");
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnRefreshMap
            // 
            this.btnRefreshMap.Image = global::HeraclitusMapLib.Properties.Resources.refresh_16;
            this.btnRefreshMap.Location = new System.Drawing.Point(3, 3);
            this.btnRefreshMap.Name = "btnRefreshMap";
            this.btnRefreshMap.Size = new System.Drawing.Size(33, 32);
            this.btnRefreshMap.TabIndex = 8;
            this.toolTip1.SetToolTip(this.btnRefreshMap, "Redraw map");
            this.btnRefreshMap.UseCompatibleTextRendering = true;
            this.btnRefreshMap.UseVisualStyleBackColor = true;
            this.btnRefreshMap.Click += new System.EventHandler(this.btnRefreshMap_Click);
            // 
            // cmbMapType
            // 
            this.cmbMapType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMapType.FormattingEnabled = true;
            this.cmbMapType.Location = new System.Drawing.Point(250, 7);
            this.cmbMapType.Name = "cmbMapType";
            this.cmbMapType.Size = new System.Drawing.Size(188, 21);
            this.cmbMapType.TabIndex = 16;
            this.cmbMapType.SelectedIndexChanged += new System.EventHandler(this.cmbMapType_SelectedIndexChanged);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::HeraclitusMapLib.Properties.Resources.nopservices_logo;
            this.pictureBox1.Location = new System.Drawing.Point(561, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 32);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 14;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::HeraclitusMapLib.Properties.Resources.anaxgroup_logo;
            this.pictureBox2.Location = new System.Drawing.Point(667, -3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(25, 38);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 15;
            this.pictureBox2.TabStop = false;
            // 
            // ButtonPanelCtl
            // 
            this.Controls.Add(this.cmbMapType);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnSaveMapAsImage);
            this.Controls.Add(this.btnCacheAllMap);
            this.Controls.Add(this.btnCenterMap);
            this.Controls.Add(this.zoomLevel);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.btnRefreshMap);
            this.Controls.Add(this.labelControl1);
            this.MaximumSize = new System.Drawing.Size(20000, 38);
            this.MinimumSize = new System.Drawing.Size(325, 38);
            this.Name = "ButtonPanelCtl";
            this.Size = new System.Drawing.Size(709, 38);
            this.Load += new System.EventHandler(this.FrmDesignPanel_Load);
            ((System.ComponentModel.ISupportInitialize)(this.zoomLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelControl1;
        private System.Windows.Forms.Button btnCenterMap;
        private System.Windows.Forms.TrackBar zoomLevel;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnRefreshMap;
        private System.Windows.Forms.Button btnCacheAllMap;
        private System.Windows.Forms.Button btnSaveMapAsImage;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ComboBox cmbMapType;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}
