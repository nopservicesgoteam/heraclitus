﻿using HeraclitusMapLib.Framework;
using HeraclitusMapLib.Map;
using HeraclitusMapLib.Map.Google;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HeraclitusMapLib.Layers
{
    public class FlightLayer : GraphicLayer
    {
        private List<Coordinate> _Data = null;
        private int _pointIndex = 0;

        private readonly ReaderWriterLockSlim _lockFr = new ReaderWriterLockSlim(LockRecursionPolicy.NoRecursion);

        private float _pointRadius = 5.0f;
        private Brush _pointBrush = new SolidBrush(Color.Yellow);
        private Font osdFont = new Font(FontFamily.GenericMonospace, 12);
        private Brush osdBrush = new SolidBrush(Color.White);

        public FlightLayer(int pWidth, int pHeight, Coordinate centerCoordinate, int pLevel) 
            : base(pWidth, pHeight, centerCoordinate, pLevel)
        {
        }

        public void ClearData()
        {
            try
            {
                _lockFr.EnterWriteLock();
                _Data = null;
            }
            finally
            {
                _lockFr.ExitWriteLock();
            }
            Update(new Rectangle(0, 0, Width, Height));
        }

        public void SetData(List<Coordinate> data)
        {
            List < Coordinate > newData = new List<Coordinate>();
            newData.AddRange(data);
            try
            {
                _lockFr.EnterWriteLock();
                _Data = newData;
                SetPointIndex(0);
            }
            finally
            {
                _lockFr.ExitWriteLock();
            }
            Update(new Rectangle(0, 0, Width, Height));
        }

        public void SetPointIndex(int newPointIndex)
        {
            if (this._Data != null && this._Data.Count > 0)
            {
                if (newPointIndex < 0) newPointIndex = 0;
                if (newPointIndex > this._Data.Count - 1) newPointIndex = this._Data.Count - 1;

            }
            else
            {
                newPointIndex = 0;
            }

            this._pointIndex = newPointIndex;

            Update(new Rectangle(0, 0, Width, Height));
        }


        protected override void DrawLayer(Rectangle clipRectangle)
        {
            try
            {
                SwapDrawBuffer();

                //!!!Draw all objects after FillTransparent (*ClipRectangle)
                FillTransparent();

                var localScreenView = (GoogleRectangle)ScreenView.Clone();

                RedrawFlight(localScreenView);
            }
            finally
            {
                SwapDrawBuffer();
            }
            FireIvalidateLayer(clipRectangle);

        }

        private void RedrawFlight(GoogleRectangle localScreenView)
        {
            try
            {
                _lockFr.EnterReadLock();

                if (_Data != null && _Data.Count > 0)
                {
                    Coordinate p1, p2;
                    p1 = _Data[0];
                    for (int i = 1; i < _Data.Count; i++)
                    {
                        p2 = _Data[i];

                        var edgeRect = new CoordinateRectangle(p1.Longitude, p1.Latitude, p2.Longitude, p2.Latitude);
                        var rect = edgeRect.GetScreenRect(localScreenView);

                        DrawLine(rect, 3, Color.Red);

                        p1 = p2;
                    }

                    p1 = _Data[this._pointIndex];
                    var pCoor = new Coordinate(p1.Longitude, p1.Latitude);
                    var pt = pCoor.GetScreenPoint(localScreenView);
                    FillEllipse(pt, _pointRadius, _pointBrush);

                }


                //DrawString("test", 5, this.Height- 25, osdBrush, osdFont);
            }
            catch (Exception ex)
            {
                //do nothing
                System.Diagnostics.Trace.WriteLine(ex.Message);
            }
            finally
            {
                _lockFr.ExitReadLock();
            }
        }

    }
}
