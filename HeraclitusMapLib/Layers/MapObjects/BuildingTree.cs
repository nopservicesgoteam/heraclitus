using System.Data;
using System.Threading.Tasks;
using HeraclitusMapLib.ExampleDb;
using HeraclitusMapLib.Layers.MapObjects.TreeNodes;
using HeraclitusMapLib.Map.Spatial;
using HeraclitusMapLib.Map.Spatial.Types;

namespace HeraclitusMapLib.Layers.MapObjects
{
    public class BuildingTree : SpatialTree<BuildingNode>
    {
        public SimpleMapDb.BuildingsDataTable BuildingDbRows { get; private set; }

        public BuildingTree()
            : base(SpatialSheetPowerTypes.Ultra, SpatialSheetPowerTypes.Extra, SpatialSheetPowerTypes.Medium, SpatialSheetPowerTypes.Low)
        {
            BuildingDbRows = new SimpleMapDb.BuildingsDataTable();
        }

        protected void Insert(SimpleMapDb.BuildingsRow row)
        {
            base.Insert(row);
        }

        protected void Delete(SimpleMapDb.BuildingsRow row)
        {
            base.Delete(row);
        }
        
        public void LoadData()
        {
            Clear();
            
            //read data from db here

            Parallel.ForEach(BuildingDbRows, Insert);
        }

        public void MergeData(SimpleMapDb.BuildingsDataTable buildings)
        {
            if (BuildingDbRows == null) return;

            BuildingDbRows.Merge(buildings, false, MissingSchemaAction.Error);

            Parallel.ForEach(buildings, row =>
            {
                var newRow = BuildingDbRows.FindByID(row.ID);
                Insert(newRow);
            });
            //apply changes to db here
        }

        public bool RemoveBuilding(int objectId)
        {
            var row = (BuildingDbRows == null) ? null : BuildingDbRows.FindByID(objectId);
            if (row != null)
            {
                Delete(row);

                BuildingDbRows.Rows.Remove(row);
                //apply changes to db here

                return true;
            }
            return false;
        }

        public SimpleMapDb.BuildingsRow GetBuilding(int objectId)
        {
            if (BuildingDbRows == null) return null;

            var row = BuildingDbRows.FindByID(objectId);
            if (row != null)
            {
                var dt = (SimpleMapDb.BuildingsDataTable)BuildingDbRows.Clone();
                dt.ImportRow(row);
                return (SimpleMapDb.BuildingsRow)dt.Rows[0];
            }
            return null;
        }
    }
}
