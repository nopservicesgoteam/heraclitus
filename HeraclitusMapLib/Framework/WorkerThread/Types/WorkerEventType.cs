namespace HeraclitusMapLib.Framework.WorkerThread.Types
{
    public enum WorkerEventType { None, RedrawLayer, DownloadImage, DrawImage, ReloadData, AddDbObject };
}