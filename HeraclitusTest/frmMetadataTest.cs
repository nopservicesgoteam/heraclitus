﻿using Heraclitus.Metadata;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HeraclitusTest
{
    public partial class frmMetadataTest : Form
    {
        private FlightMetadata flightMetadata;
        
        public frmMetadataTest()
        {
            InitializeComponent();

            this.flightMetadata = new FlightMetadata(null, pictureBox1);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string errmsg = flightMetadata.Load("C:\\Devlopment\\NopServices\\Hearclitus\\examples\\flight-metadata\\AIT-09 (Mission 04) 2018-09-15_10-13-49_v2.csv");

            if (errmsg != null) MessageBox.Show(string.Format("Error loading flight metadata file:\r\n{0}", errmsg), "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);                        
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //flightMetadata.MoveToSeconds(curTimeSec);
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            double gotoTime = trackBar1.Value * flightMetadata.MaxTimeMilliseconds / trackBar1.Maximum;
            flightMetadata.MoveToTimeMilliseconds(gotoTime);
        }
    }
}
