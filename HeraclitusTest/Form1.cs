﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Features2D;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Emgu.CV.XFeatures2D;
using Hearclitus.ImageProcessing;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HeraclitusTest
{
    public partial class Form1 : Form
    {
        MotionDetectorWithBackgroundWarping motionDetectorWBW = new MotionDetectorWithBackgroundWarping();
        Mat modelPic, observedPic, warpedPic, motionDetectPic;
        Mat out1, out2;
        Mat frame1, frame2;

        public Form1()
        {
            InitializeComponent();
        }

        int picToggleIndex = -1;
        private void timer1_Tick(object sender, EventArgs e)
        {
            picToggleIndex = (picToggleIndex + 1) % 2;
            switch (picToggleIndex)
            {
                case 0:
                    pictureBox1.Image = out1.Bitmap;
                    pictureBox3.Image = warpedPic.Bitmap;
                    pictureBox5.Image = motionDetectorWBW.PrevFrame.Bitmap; //frame1.Bitmap;
                    break;

                case 1:
                    pictureBox1.Image = out2.Bitmap;
                    pictureBox3.Image = observedPic.Bitmap;
                    pictureBox5.Image = motionDetectorWBW.CurFrame.Bitmap;
                    break;
            }
        }

        /*
            W=1920, H=1080
            pixels=2073600
            features=1430
            warping=202ms
            motion-detection=38ms


            W=960, H=540
            pixels=518400
            features=566
            warping=124ms
            motion-detection=13ms


            W=640, H=360
            pixels=230400
            features=1291
            warping=86ms
            motion-detection=10ms
         */

        private void Form1_Load(object sender, EventArgs e)
        {
            motionDetectorWBW.Options = MotionDetectorWithBackgroundWarpingOptions.GetOptionsFor960();

            frame1 = new Mat("C:\\Devlopment\\NopServices\\Hearclitus\\examples\\frames\\frame_198.bmp");
            frame2 = new Mat("C:\\Devlopment\\NopServices\\Hearclitus\\examples\\frames\\frame_200.bmp");

            motionDetectorWBW.Setup(frame1);
            motionDetectorWBW.Feed(frame1);
            motionDetectorWBW.Feed(frame2);
            motionDetectorWBW.MotionDetector.DrawMatchedBoxes(motionDetectorWBW.WarpedPrevFrame);
            motionDetectorWBW.MotionDetector.DrawMatchedBoxes(motionDetectorWBW.PrevFrame);
            motionDetectorWBW.MotionDetector.DrawMatchedBoxes(motionDetectorWBW.CurFrame);

            //motionDetectorWBW.Finish();


            Mat modelPicSrc = new Mat("C:\\Devlopment\\NopServices\\Hearclitus\\examples\\frames\\frame_198.bmp");
            Mat observedPicSrc = new Mat("C:\\Devlopment\\NopServices\\Hearclitus\\examples\\frames\\frame_200.bmp");

            double scaleF = 1/1.0;
            int featuresDetectorThreshold = 105;
            int motionDetectorThreshold = 192;

            //double scaleF = 1 / 2.0;
            //int featuresDetectorThreshold = 85;
            //int motionDetectorThreshold = 128;

            //double scaleF = 1 / 3.0;
            //int featuresDetectorThreshold = 55;
            //int motionDetectorThreshold = 128;


            int newWidth = (int)(scaleF * modelPicSrc.Width);
            int newHeight = (int)(scaleF * modelPicSrc.Height);
            Size newSize = new Size(newWidth, newHeight);

            modelPic = new Mat(newSize, modelPicSrc.Depth, modelPicSrc.NumberOfChannels);
            observedPic = new Mat(newSize, modelPicSrc.Depth, modelPicSrc.NumberOfChannels);


            Mat blurredPic = new Mat(newSize, modelPicSrc.Depth, modelPicSrc.NumberOfChannels);

            double kernelSize = Math.Ceiling(1.0 / scaleF);
            if (kernelSize % 2 == 0) kernelSize += 1;
            Size blurKernelSize = new Size((int)kernelSize, (int)kernelSize);
            Point anchorPoint = new Point((int)Math.Ceiling(kernelSize / 2.0) - 1, (int)Math.Ceiling(kernelSize / 2.0) - 1);

            CvInvoke.Blur(modelPicSrc, blurredPic, blurKernelSize, anchorPoint);
            CvInvoke.Resize(blurredPic, modelPic, newSize);

            CvInvoke.Blur(observedPicSrc, blurredPic, blurKernelSize, anchorPoint);
            CvInvoke.Resize(blurredPic, observedPic, newSize);

            Console.WriteLine("W={0}, H={1}\r\npixels={2}", newWidth, newHeight, newWidth * newHeight);

            Stopwatch sw = new Stopwatch();
            sw.Reset();
            sw.Start();

            out1 = new Mat(modelPic.Width, observedPic.Height, modelPic.Depth, modelPic.NumberOfChannels);
            out2 = new Mat(observedPic.Width, observedPic.Height, observedPic.Depth, observedPic.NumberOfChannels);
            
            FastDetector fastDetector = new FastDetector(featuresDetectorThreshold);
            VectorOfKeyPoint modelKeyPoints = new VectorOfKeyPoint(); 
            VectorOfKeyPoint observedKeyPoints = new VectorOfKeyPoint();

            fastDetector.DetectRaw(modelPic, modelKeyPoints);
            fastDetector.DetectRaw(observedPic, observedKeyPoints);

            Console.WriteLine("features={0}", modelKeyPoints.Size);

            BriefDescriptorExtractor descriptor = new BriefDescriptorExtractor();

            Mat modelDescriptors = new Mat();
            Mat observedDescriptors = new Mat();

            descriptor.Compute(modelPic, modelKeyPoints, modelDescriptors);
            descriptor.Compute(observedPic, observedKeyPoints, observedDescriptors);


            Bgr featuresColor = new Bgr(0, 0, 255);
            Features2DToolbox.DrawKeypoints(modelPic, modelKeyPoints, out1, featuresColor);
            Features2DToolbox.DrawKeypoints(observedPic, observedKeyPoints, out2, featuresColor);

            //Do Mathcing
            Mat mask;
            int k = 2;
            double uniquenessThreshold = 0.8;

            BFMatcher matcher = new BFMatcher(DistanceType.L2);
            matcher.Add(modelDescriptors);

            VectorOfVectorOfDMatch matches = new VectorOfVectorOfDMatch();
            matcher.KnnMatch(observedDescriptors, matches, k, null);
            mask = new Mat(matches.Size, 1, Emgu.CV.CvEnum.DepthType.Cv8U, 1);
            mask.SetTo(new MCvScalar(255));
            Features2DToolbox.VoteForUniqueness(matches, uniquenessThreshold, mask);

            Mat homography = null;
            int nonZeroCount = CvInvoke.CountNonZero(mask);
            if (nonZeroCount >= 4)
            {
                nonZeroCount = Features2DToolbox.VoteForSizeAndOrientation(modelKeyPoints, observedKeyPoints, matches, mask, 1.5, 20);
                if (nonZeroCount >= 4)
                    homography = Features2DToolbox.GetHomographyMatrixFromMatchedFeatures(modelKeyPoints, observedKeyPoints, matches, mask, 2);
            }

            warpedPic = new Mat();
            CvInvoke.WarpPerspective(modelPic, warpedPic, homography, modelPic.Size);

            sw.Stop();

            long millisWarping = sw.ElapsedMilliseconds;

            sw.Reset();
            sw.Start();

            Mat observedGrayPic = new Mat();
            Mat warpedGrayPic = new Mat();

            CvInvoke.CvtColor(observedPic, observedGrayPic, ColorConversion.Bgr2Gray);
            CvInvoke.CvtColor(warpedPic, warpedGrayPic, ColorConversion.Bgr2Gray);

            Mat diffPic = new Mat();
            CvInvoke.AbsDiff(observedGrayPic, warpedGrayPic, diffPic);

            Mat threshPic = new Mat();
            CvInvoke.Threshold(diffPic, threshPic, motionDetectorThreshold, 255, ThresholdType.Binary);

            Mat dilatePic = new Mat();
            Point dilateAnchor = new Point(-1, -1);
            MCvScalar dilateBorder = new MCvScalar();
            CvInvoke.Dilate(threshPic, dilatePic, null, dilateAnchor, 2*3*4, BorderType.Default, dilateBorder);

            VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint();
            CvInvoke.FindContours(dilatePic, contours, null, RetrType.External, ChainApproxMethod.ChainApproxSimple);
            VectorOfPoint curContour;
            Rectangle contourBBox;
            MCvScalar bboxColor = new MCvScalar(0, 0, 255);
            double contourArea;
            for (int i = 0; i < contours.Size; i++)
            {
                curContour = contours[i];
                contourArea = CvInvoke.ContourArea(curContour);
                if (contourArea > 1 && contourArea < 50000)
                {
                    contourBBox = CvInvoke.BoundingRectangle(curContour);
                    CvInvoke.Rectangle(modelPic, contourBBox, bboxColor, 4);
                    CvInvoke.Rectangle(warpedPic, contourBBox, bboxColor, 4);
                    CvInvoke.Rectangle(observedPic, contourBBox, bboxColor, 4);
                }
            }

            sw.Stop();

            long milliMotionDetection = sw.ElapsedMilliseconds;

            Console.WriteLine("warping={0}ms\r\nmotion-detection={1}ms", millisWarping, milliMotionDetection);

            pictureBox4.Image = dilatePic.Bitmap;


            Mat result = new Mat();
            MCvScalar pointColor = new MCvScalar(0, 0, 255);
            MCvScalar matchColor = new MCvScalar(0, 255, 0);
            Features2DToolbox.DrawMatches(modelPic, modelKeyPoints, observedPic, observedKeyPoints, matches, result, matchColor, pointColor, mask, Features2DToolbox.KeypointDrawType.Default);

            pictureBox2.Image = result.Bitmap;


            //Image<Bgr, byte>  diffPic = new Image<Bgr, byte>(pic1.Width, pic2.Height);
            //CvInvoke.AbsDiff(pic1, pic2, diffPic);
        }
    }
}
