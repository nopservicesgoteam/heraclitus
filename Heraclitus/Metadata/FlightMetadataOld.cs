﻿using Heraclitus.Geometry;
using HeraclitusMapLib.Controls;
using HeraclitusMapLib.Map;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Heraclitus.Metadata
{
    public class FlightMetadataOld
    {
        public List<FlightPositionInfo> Data = new List<FlightPositionInfo>();
        public double MaxTimeMilliseconds = 0;
        public Bounds Bounds = new Bounds();
        public Bounds BoundsProjected = new Bounds();
        public double RefreshPeriod = 0.05; //every so many seconds

        private Bitmap _pathImage;
        private int _pointIndex = 0;
        private double _relativeTimeOffset = 0;

        private int _imW = 500;
        private int _imH = 500;
        private int _imPadding = 50;
        private float _pointRadius = 5.0f;
        private Pen _pathPen = new Pen(new SolidBrush(Color.Red), 3);
        private Brush _pointBrush = new SolidBrush(Color.Yellow);
        private Font osdFont = new Font(FontFamily.GenericMonospace, 14);
        private Brush osdBrush = new SolidBrush(Color.White);

        private double _minX;
        private double _maxY;
        private double _drawFactor;

        private MapCtl _mapCtl;
        private PictureBox _pictureBox;

        public FlightMetadataOld(MapCtl mapCtl, PictureBox pictureBox)
        {
            this._mapCtl = mapCtl;
            this._pictureBox = pictureBox;
        }

        public void ClearData()
        {
            this.Bounds.FoundMinMax = false;
            this.BoundsProjected.FoundMinMax = false;
            Data.Clear();
            MaxTimeMilliseconds = 0;
            if (this._mapCtl != null)
            {
                this._mapCtl.ClearFlightData();
            }
            _pathImage = null;
            this.SetPointIndex(0);
        }

        public string Load(string filepath)
        {
            this.ClearData();

            if (!File.Exists(filepath)) return "file does not exists";

            string fileext = Path.GetExtension(filepath).ToLower();

            if (!fileext.Equals(".csv")) return "only .csv files are accepted";

            List<Coordinate> coordinatesList = new List<Coordinate>();

            string[] lines = File.ReadAllLines(filepath);
            int Nlines = lines.Length;
            int Ncols = 0;
            string line;
            string[] cols;
            string[] colNames = null;
            FlightPositionInfo curPosition = null;
            double minLat=0, maxLat = 0;
            double minLon = 0, maxLon = 0;
            double minX = 0, maxX = 0;
            double minY = 0, maxY = 0;
            for (int i = 0; i < Nlines; i++)
            {
                line = lines[i];
                cols = line.Split(',');
                if (i == 0)
                {
                    //Header lines
                    Ncols = cols.Length;
                    colNames = new string[Ncols];
                    cols.CopyTo(colNames, 0);
                }
                else
                {
                    if (Ncols != cols.Length)
                    {
                        return string.Format("line {0} should contain {1} columns (contains {2} instead)", i+1, Ncols, cols.Length);
                    }
                    curPosition = new FlightPositionInfo(cols, colNames);
                    this.Data.Add(curPosition);
                    coordinatesList.Add(new Coordinate(curPosition.Longitude, curPosition.Latitude));

                    this.Bounds.UpdateMinMax(curPosition.Latitude, curPosition.Longitude);
                    this.BoundsProjected.UpdateMinMax(curPosition.Y, curPosition.X);
                }
            }
            if (curPosition != null)
            {
                MaxTimeMilliseconds = curPosition.TimeMilliseconds;
            }

            this._pointIndex = 0;
            GeneratePathImage();

            this.SetPointIndex(0);

            if (this._mapCtl != null)
            {
                this._mapCtl.SetFlightData(coordinatesList);
                this._mapCtl.CenterCoordinate = new Coordinate( (this.Bounds.minLon + this.Bounds.maxLon) / 2.0, (this.Bounds.minLat + this.Bounds.maxLat) / 2.0); ;
            }

            return null;
        }
        
        private PointF GetDrawPoint(FlightPositionInfo position)
        {            
            return new PointF(
                    (float)(_imPadding + _drawFactor * (position.X - _minX)),
                    (float)(_imPadding + _drawFactor * (_maxY - position.Y))
                    );
        }

        private void GeneratePathImage()
        {

            _pathImage = new Bitmap(_imW, _imH);

            Graphics g = Graphics.FromImage(_pathImage);

            Bitmap mapBackground = new Bitmap(Heraclitus.Properties.Resources.map_background);

            g.Clear(Color.FloralWhite);

            g.DrawImage(mapBackground, new Rectangle(0, 0, _imW, _imH));

            _minX = this.BoundsProjected.minLon;
            _maxY = this.BoundsProjected.maxLat;

            double latRange = this.BoundsProjected.GetLatRange();
            double lonRange = this.BoundsProjected.GetLonRange();

            double coorRange = Math.Max(latRange, lonRange);

            _drawFactor = (_imW - 2 * _imPadding) / coorRange;
            
            PointF prevDrawPoint = PointF.Empty;
            PointF curDrawPoint = PointF.Empty;

            for (int i = 0; i < this.Data.Count; i++)
            {


                curDrawPoint = GetDrawPoint(this.Data[i]);
            
                if (i > 0)
                {                    
                    g.DrawLine(_pathPen, prevDrawPoint, curDrawPoint);
                }

                prevDrawPoint = curDrawPoint;
            }            
        }

        private double _lastRefreshTime = 0;
        public void SetPointIndex(int newPointIndex)
        {
            if (this.Data != null && this.Data.Count > 0)
            {
                if (newPointIndex < 0) newPointIndex = 0;
                if (newPointIndex > this.Data.Count - 1) newPointIndex = this.Data.Count - 1;

            }
            else
            {
                newPointIndex = 0;
            }


            this._pointIndex = newPointIndex;
            //Console.WriteLine("#{0}/{1}", this._pointIndex, this.Data.Count);

            double curtm = DateTime.Now.ToOADate() * 86400;
            if (curtm - _lastRefreshTime >= RefreshPeriod)
            {
                if (this._pictureBox != null)
                {
                    this._pictureBox.Invoke(new Action(() => {
                        this._pictureBox.Image = this.GetCurrentImage();
                    }));
                }
                _lastRefreshTime = curtm;
            }

            if (this._mapCtl != null)
            {
                this._mapCtl.SetFlightPointIndex(this._pointIndex);
            }

        }

        public void IncrementPointIndex()
        {
            int newPoint = 0;

            if (this.Data != null && this.Data.Count > 0)
            {
                newPoint = this._pointIndex + 1;
                if (newPoint >= this.Data.Count - 1) newPoint = 0;
            }

            this.SetPointIndex(newPoint);
        }

        public void SetRelativeOffset(double relativeTimeOffset)
        {
            if (relativeTimeOffset < 0) relativeTimeOffset = 0;
            if (relativeTimeOffset > 1) relativeTimeOffset = 1;

            this._relativeTimeOffset = relativeTimeOffset;
            this.MoveToTimeMilliseconds(0);
        }

        public void MoveToTimeMilliseconds(double gotoTime)
        {
            if (this.Data == null) return;
            if (this.Data.Count == 0) return;

            int idx = 0;
            FlightPositionInfo position, curPosition;
            curPosition = this.Data[this._pointIndex];

            gotoTime += this._relativeTimeOffset * this.MaxTimeMilliseconds;
            if (gotoTime < 0) gotoTime = 0;
            if (gotoTime > this.MaxTimeMilliseconds) gotoTime = this.MaxTimeMilliseconds;


            bool moveForward = (gotoTime >= curPosition.TimeMilliseconds);
            if (moveForward)
            {
                for (int i = 0; i < this.Data.Count; i++)
                {
                    idx = (i + this._pointIndex) % this.Data.Count;
                    position = this.Data[idx];
                    if (position.TimeMilliseconds >= gotoTime)
                    {
                        this.SetPointIndex(idx);
                        break;
                    }
                }
            }else
            {
                for (int i = this.Data.Count-1; i >= 0; i--)
                {
                    idx = (i + this._pointIndex) % this.Data.Count;
                    position = this.Data[idx];
                    if (position.TimeMilliseconds <= gotoTime)
                    {
                        this.SetPointIndex(idx);
                        break;
                    }
                }
            }
        }

        public FlightPositionInfo GetCurrentInfo()
        {
            if (_pointIndex >= 0 && _pointIndex < this.Data.Count)
            {
                FlightPositionInfo position = this.Data[_pointIndex];
                return position;
            }
            else
            {
                return null;
            }
        }


        public Bitmap GetCurrentImage()
        {
            const int linesep = 18;

            if (_pathImage == null) return null;

            Bitmap bmp = new Bitmap(_pathImage);

            Graphics g = Graphics.FromImage(bmp);

            if (_pointIndex >= 0 && _pointIndex < this.Data.Count)
            {
                FlightPositionInfo position = this.Data[_pointIndex];

                PointF curDrawPoint = GetDrawPoint(position);
                g.FillEllipse(_pointBrush, curDrawPoint.X - _pointRadius, curDrawPoint.Y - _pointRadius, 2 * _pointRadius, 2 * _pointRadius);

                g.DrawString(position.Time.ToString("dd/MM/yyyy hh:mm:ss.fff"), osdFont, osdBrush, 10, 10);
                g.DrawString(string.Format("lat: {0:0.00000}", position.Latitude), osdFont, osdBrush, 10, 10 + 1 * linesep);
                g.DrawString(string.Format("lon: {0:0.00000}", position.Longitude), osdFont, osdBrush, 10, 10 + 2 * linesep);


                g.DrawString(string.Format("  Alt: {0:0.0}m", position.Altitude), osdFont, osdBrush, 10, _imH - 10 - 1* linesep);
                g.DrawString(string.Format("Speed: {0:0.0}km/h", position.Speed), osdFont, osdBrush, 10, _imH - 10 - 2* linesep);
                g.DrawString(string.Format("  Yaw: {0:0.0}", position.Yaw), osdFont, osdBrush, 10, _imH - 10 - 3 * linesep);
                g.DrawString(string.Format(" Roll: {0:0.0}", position.Roll), osdFont, osdBrush, 10, _imH - 10 - 4 * linesep);
                g.DrawString(string.Format("Pitch: {0:0.0}", position.Pitch), osdFont, osdBrush, 10, _imH - 10 - 5 * linesep);
            }

            return bmp;
        }

    }

}
