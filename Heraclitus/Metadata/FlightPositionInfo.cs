﻿using Heraclitus.Geometry;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heraclitus.Metadata
{
    public class FlightPositionInfo
    {
        private static CultureInfo CultureEnglishUSA = CultureInfo.CreateSpecificCulture("en-US");

        public Dictionary<string, string> Values = new Dictionary<string, string>();

        public double Latitude;
        public double Longitude;
        public double Altitude;
        public double Speed;
        public double Pitch;
        public double Roll;
        public double Yaw;
        public double TimeMilliseconds;
        public DateTime Time;

        public double X;
        public double Y;

        public FlightPositionInfo()
        {

        }

        private double str2double(string sval)
        {
            double res = 0;
            if (!double.TryParse(sval, NumberStyles.Number, CultureEnglishUSA, out res)) res = 0;
            return res;
        }

        private DateTime str2time(string strdate)
        {
            DateTime res = DateTime.MinValue;
            if (!DateTime.TryParseExact(strdate, "yyyy-MM-dd hh:mm:ss.fff", CultureEnglishUSA, DateTimeStyles.None, out res)) res = DateTime.MinValue;
            return res;
        }

        public FlightPositionInfo(string[] cols, string[] colNames)
        {
            if (cols.Length != colNames.Length) throw new Exception("cols and colNames differ");

            string curColName;
            string curValue;
            for (int j = 0; j < colNames.Length; j++)
            {
                curColName = colNames[j];
                curValue = cols[j];                
                switch (curColName)
                {
                    case "latitude":
                        this.Latitude = str2double(curValue);
                        break;

                    case "longitude":
                        this.Longitude = str2double(curValue);
                        break;

                    case "altitude(m)":
                        this.Altitude = str2double(curValue);
                        break;

                    case "speed(mps)":
                        this.Speed = 3.6*str2double(curValue);
                        break;

                    case "pitch(deg)":
                        this.Pitch = str2double(curValue);
                        break;

                    case "roll(deg)":
                        this.Roll = str2double(curValue);
                        break;

                    case "yaw(deg)":
                        this.Yaw = str2double(curValue);
                        break;

                    case "time(millisecond)":
                        this.TimeMilliseconds = str2double(curValue);
                        break;

                    case "datetime(local)":
                        this.Time = str2time(curValue);
                        break;

                    default:
                        //this.Values.Add(curColName, curValue);
                        break;
                }
            }

            this.X = FastMercator.LonToX(this.Longitude);
            this.Y = FastMercator.LatToY(this.Latitude);

            double checkLat = FastMercator.YToLat(this.Y);
            double checkLon = FastMercator.XToLon(this.X);
        }
        //latitude,longitude,altitude(m),
        //ultrasonicHeight(m),
        //speed(mps),
        //distance(m),max_altitude(m),max_ascent(m),max_speed(mps),max_distance(m),
        //time(millisecond),datetime(utc),datetime(local),
        //satellites,pressure(Pa),temperature(F),voltage(v),home_latitude,home_longitude,velocityX(mps),velocityY(mps),velocityZ(mps),
        //pitch(deg),roll(deg),yaw(deg),powerlevel,isflying,istakingphoto,remainPowerPercent,remainLifePercent,currentCurrent,currentElectricity,currentVoltage,batteryTemperature,dischargeCount,flightmode,isMotorsOn,isTakingVideo,Rc_elevator,Rc_aileron,Rc_throttle,Rc_rudder,Rc_gyro,timestamp,Battery_Cell1,Battery_Cell2,Battery_Cell3,Battery_Cell4,Battery_Cell5,Battery_Cell6,Dronetype,AppVersion,Planename,FlyControllerSerialNumber,RemoteSerialNumber,BatterySerialNumber,CENTER_BATTERY.productDate,CENTER_BATTERY.serialNo,CENTER_BATTERY.fullCapacity,CENTER_BATTERY.productDateRaw,pitchRaw,rollRaw,yawRaw,gimbalPitchRaw,gimbalRollRaw,gimbalYawRaw,flyState,altitudeRaw,speedRaw,distanceRaw,velocityXRaw,velocityYRaw,velocityZRaw,data_reuse,App_Tip,App_Warning,downlinkSignalQuality,uplinkSignalQuality,transmissionChannel,rcModeSwitch,rcHome,rcPause,rcTransform,rcJoystickMode,appMode,FlyControllerSerialNumber_legacy,RemoteSerialNumber_legacy,BatterySerialNumber_legacy,Battery2SerialNumber,frontAvoidDist,backAvoidDist,isBrakingToAvoid


        //38.598033,21.316547,32.30,
        //0.00,
        //3.90,
        //0.10,32.30,32.30,4.60,0.26,
        //9966,2018-09-15 07:13:59.251,2018-09-15 10:13:59.252,
        //11,0,0,12.565,38.598034,21.316547,0.00,0.00,-3.90,
        //0.10,0.20,-49.90,
        //0,1,0,100,99,-10430,3626,12565,3002.50,14,P_GPS,1,0,0,0,660,0,0,1536995639251,4172,4172,4160,0,0,0,13,,,,,,,0,0,0,-1.00,2.00,-499.00,-318.00,0.00,-499.00,6,323.00,39.00,0.97,0.00,0.00,-39.00,0,,,100,100,0,1,0,0,0,2,0,,,,,0.0,0.0,0

    }
}
