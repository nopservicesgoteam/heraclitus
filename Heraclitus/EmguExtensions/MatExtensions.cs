﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Hearclitus.EmguExtensions
{
    public static class MatExtensions
    {

        public static dynamic GetValue(this Mat mat, int row, int col)
        {
            var value = CreateElements(mat.Depth, mat.ElementSize);
            Marshal.Copy(mat.DataPointer + (row * mat.Cols + col) * mat.ElementSize, value, 0, mat.ElementSize);
            return value;
        }

        /*
        public static void SetValue(this Mat mat, int row, int col, dynamic value)
        {
            var target = CreateElement(mat.Depth, value);
            Marshal.Copy(target, 0, mat.DataPointer + (row * mat.Cols + col) * mat.ElementSize, 1);
        }
        private static dynamic CreateElement(DepthType depthType, dynamic value)
        {
            var element = CreateElement(depthType);
            element[0] = value;
            return element;
        }*/

        private static dynamic CreateElements(DepthType depthType, int elementSize)
        {
            if (depthType == DepthType.Cv8S)
            {
                return new sbyte[elementSize];
            }
            if (depthType == DepthType.Cv8U)
            {
                return new byte[elementSize];
            }
            if (depthType == DepthType.Cv16S)
            {
                return new short[elementSize];
            }
            if (depthType == DepthType.Cv16U)
            {
                return new ushort[elementSize];
            }
            if (depthType == DepthType.Cv32S)
            {
                return new int[elementSize];
            }
            if (depthType == DepthType.Cv32F)
            {
                return new float[elementSize];
            }
            if (depthType == DepthType.Cv64F)
            {
                return new double[elementSize];
            }
            return new float[elementSize];
        }

    }
}
