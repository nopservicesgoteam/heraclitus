﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Heraclitus
{
    public partial class FormAbout : Form
    {
        public FormAbout()
        {
            InitializeComponent();

            Assembly ass = System.Reflection.Assembly.GetExecutingAssembly();
            AssemblyName assname = ass.GetName();

            labelVersion.Text = String.Format("{0} v{1}.{2}.{3}r{4}", assname.Name, assname.Version.Major, assname.Version.Minor, assname.Version.Build, assname.Version.Revision);
        }

        private void pictureBoxLogoAnax_MouseClick(object sender, MouseEventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.anax-group.com");
        }

        private void pictureBoxLogoNop_MouseClick(object sender, MouseEventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.nopservices.com");
        }

        private void FormAbout_KeyPress(object sender, KeyPressEventArgs e)
        {
            switch (e.KeyChar)
            {
                case (char)27:
                    this.Close();
                    break;

                default:
                    break;
            }
        }
    }
}
