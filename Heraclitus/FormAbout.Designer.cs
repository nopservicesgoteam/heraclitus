﻿namespace Heraclitus
{
    partial class FormAbout
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAbout));
            this.labelAppName = new System.Windows.Forms.Label();
            this.labelVersion = new System.Windows.Forms.Label();
            this.labelText = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBoxLogoNop = new System.Windows.Forms.PictureBox();
            this.pictureBoxLogoAnax = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogoNop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogoAnax)).BeginInit();
            this.SuspendLayout();
            // 
            // labelAppName
            // 
            this.labelAppName.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelAppName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.labelAppName.Location = new System.Drawing.Point(10, 10);
            this.labelAppName.Name = "labelAppName";
            this.labelAppName.Size = new System.Drawing.Size(390, 20);
            this.labelAppName.TabIndex = 2;
            this.labelAppName.Text = "Heraclitus";
            this.labelAppName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelVersion
            // 
            this.labelVersion.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.labelVersion.Location = new System.Drawing.Point(10, 30);
            this.labelVersion.Name = "labelVersion";
            this.labelVersion.Size = new System.Drawing.Size(390, 20);
            this.labelVersion.TabIndex = 3;
            this.labelVersion.Text = "version 1.0.1";
            this.labelVersion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelText
            // 
            this.labelText.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelText.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.labelText.Location = new System.Drawing.Point(10, 50);
            this.labelText.Name = "labelText";
            this.labelText.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.labelText.Size = new System.Drawing.Size(390, 203);
            this.labelText.TabIndex = 4;
            this.labelText.Text = resources.GetString("labelText.Text");
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Heraclitus.Properties.Resources.heraclitus_logo;
            this.pictureBox3.Location = new System.Drawing.Point(13, 258);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(105, 96);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 5;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBoxLogoNop
            // 
            this.pictureBoxLogoNop.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBoxLogoNop.Image = global::Heraclitus.Properties.Resources.nopservices_logo;
            this.pictureBoxLogoNop.Location = new System.Drawing.Point(197, 294);
            this.pictureBoxLogoNop.Name = "pictureBoxLogoNop";
            this.pictureBoxLogoNop.Size = new System.Drawing.Size(200, 60);
            this.pictureBoxLogoNop.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxLogoNop.TabIndex = 1;
            this.pictureBoxLogoNop.TabStop = false;
            this.pictureBoxLogoNop.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBoxLogoNop_MouseClick);
            // 
            // pictureBoxLogoAnax
            // 
            this.pictureBoxLogoAnax.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBoxLogoAnax.Image = global::Heraclitus.Properties.Resources.anaxgroup_logo;
            this.pictureBoxLogoAnax.Location = new System.Drawing.Point(124, 258);
            this.pictureBoxLogoAnax.Name = "pictureBoxLogoAnax";
            this.pictureBoxLogoAnax.Size = new System.Drawing.Size(67, 96);
            this.pictureBoxLogoAnax.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxLogoAnax.TabIndex = 0;
            this.pictureBoxLogoAnax.TabStop = false;
            this.pictureBoxLogoAnax.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBoxLogoAnax_MouseClick);
            // 
            // FormAbout
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(410, 367);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.labelText);
            this.Controls.Add(this.labelVersion);
            this.Controls.Add(this.labelAppName);
            this.Controls.Add(this.pictureBoxLogoNop);
            this.Controls.Add(this.pictureBoxLogoAnax);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormAbout";
            this.Padding = new System.Windows.Forms.Padding(10);
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "FormAbout";
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FormAbout_KeyPress);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogoNop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogoAnax)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxLogoAnax;
        private System.Windows.Forms.PictureBox pictureBoxLogoNop;
        private System.Windows.Forms.Label labelAppName;
        private System.Windows.Forms.Label labelVersion;
        private System.Windows.Forms.Label labelText;
        private System.Windows.Forms.PictureBox pictureBox3;
    }
}