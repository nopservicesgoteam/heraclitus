﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Hearclitus.EmguExtensions;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hearclitus.ImageProcessing
{
    public class SimpleHueDetectorOptions
    {
        public bool EnableHueDetection = false;        
        public MCvScalar hsvColor = new MCvScalar();
        public MCvScalar BoxColor = new MCvScalar(0, 0, 255);
        public int BoxThickness = 2;
        public double MinArea = 1;
        public double MaxArea = 500000;

        public SimpleHueDetectorOptions()
        {

        }
    }

    public class SimpleHueDetector
    {
        public SimpleHueDetectorOptions Options = new SimpleHueDetectorOptions();
        public List<MotionDetectionMatch> Matches = new List<MotionDetectionMatch>();

        // Lower and Upper bounds for range checking in HSV color space
        private MCvScalar mLowerBound = new MCvScalar(0);
        private MCvScalar mUpperBound = new MCvScalar(0);

        // Minimum contour area in percent for contours filtering
        private static double mMinContourArea = 0.05;

        // Color radius for range checking in HSV color space
        private MCvScalar mColorRadius = new MCvScalar(25, 50, 50, 0);
        private Mat mSpectrum;
        private VectorOfVectorOfPoint mContours;

        private Color mSelectedColor;

        // Cache
        Mat mPyrDownMat;
        Mat mHsvMat;
        Mat mMask;
        Mat mDilatedMask;
        Mat mHierarchy;

        private Point _dilateAnchor = new Point(-1, -1);
        private MCvScalar _dilateBorder = new MCvScalar();

        public SimpleHueDetector()
        {
            this.Configure();

            //roof color
            //this.SetHsvColor(Color.FromArgb(245, 174, 178));            

            //grass color
            this.SetHsvColor(Color.FromArgb(249, 175, 177));            
        }

        public Color GetSelectedColor()
        {
            return mSelectedColor;
        }

        public void Configure()
        {
            mContours = new VectorOfVectorOfPoint();
            mSpectrum = new Mat();
            mPyrDownMat = new Mat();
            mHsvMat = new Mat();
            mMask = new Mat();
            mDilatedMask = new Mat();
            mHierarchy = new Mat();
        }

    public void Finish()
        {
            if (mContours != null)
            {
                mContours.Dispose();
                mContours = null;
            }

            if (mSpectrum != null)
            {
                mSpectrum.Dispose();
                mSpectrum = null;
            }

            if (mPyrDownMat != null)
            {
                mPyrDownMat.Dispose();
                mPyrDownMat = null;
            }

            if (mHsvMat != null)
            {
                mHsvMat.Dispose();
                mHsvMat = null;
            }

            if (mMask != null)
            {
                mMask.Dispose();
                mMask = null;
            }

            if (mDilatedMask != null)
            {
                mDilatedMask.Dispose();
                mDilatedMask = null;
            }

            if (mHierarchy != null)
            {
                mHierarchy.Dispose();
                mHierarchy = null;
            }
        }

        public void SetHsvColor(Color color)
        {
            mSelectedColor = color;
            Mat rgbImg = new Mat(1, 1, DepthType.Cv8U, 3);
            rgbImg.SetTo(new MCvScalar(color.R, color.G, color.B));
            Mat hsvImg = new Mat(1, 1, DepthType.Cv16U, 3);
            CvInvoke.CvtColor(rgbImg, hsvImg, ColorConversion.Rgb2HsvFull);
            this.Options.hsvColor = CvInvoke.Sum(hsvImg);

            double minH = (this.Options.hsvColor.V0 >= mColorRadius.V0) ? this.Options.hsvColor.V0 - mColorRadius.V0 : 0;
            double maxH = (this.Options.hsvColor.V0 + mColorRadius.V0 <= 255) ? this.Options.hsvColor.V0 + mColorRadius.V0 : 255;

            mLowerBound.V0 = minH;
            mUpperBound.V0 = maxH;

            mLowerBound.V1 = this.Options.hsvColor.V1 - mColorRadius.V1;
            mUpperBound.V1 = this.Options.hsvColor.V1 + mColorRadius.V1;

            mLowerBound.V2 = this.Options.hsvColor.V2 - mColorRadius.V2;
            mUpperBound.V2 = this.Options.hsvColor.V2 + mColorRadius.V2;

            mLowerBound.V3 = 0;
            mUpperBound.V3 = 255;

            //Mat spectrumHsv = new Mat(1, (int)(maxH - minH), DepthType.Cv8U, 3);
            //spectrumHsv.SetTo(new MCvScalar(0, 0, 0));
            byte[,,] hsvSpectrumBytes = new byte[1, (int)(maxH - minH), 3];            

            for (int j = 0; j < maxH - minH; j++)
            {
                //byte[] tmp = { (byte)(minH + j), (byte)255, (byte)255 };
                //spectrumHsv.Data.SetValue(tmp, 0, j);
                hsvSpectrumBytes[0, j, 2] = (byte)100; // (minH + j);
                hsvSpectrumBytes[0, j, 1] = (byte)255;
                hsvSpectrumBytes[0, j, 0] = (byte)255;
            }

            Image<Rgb, Byte> spectrumHsv = new Image<Rgb, byte>(hsvSpectrumBytes);

            CvInvoke.CvtColor(spectrumHsv, mSpectrum, ColorConversion.Hsv2RgbFull, 4);
        }
        

        public Mat Spectrum
        {
            get
            {
                return mSpectrum;
            }
        }

        public double MinContourArea
        {
            get
            {
                return mMinContourArea;
            }
            set
            {
                mMinContourArea = value;
            }            
        }

        public void Compute(Mat curFrame)
        {
            Matches.Clear();

            CvInvoke.PyrDown(curFrame, mPyrDownMat);
            //CvInvoke.PyrDown(mPyrDownMat, mPyrDownMat);

            CvInvoke.CvtColor(mPyrDownMat, mHsvMat, ColorConversion.Bgr2HsvFull);

            //dynamic vRGB = mPyrDownMat.GetValue(0, 0);
            //dynamic vHSV = mHsvMat.GetValue(0, 0);

            CvInvoke.InRange(mHsvMat, new ScalarArray(mLowerBound), new ScalarArray(mUpperBound), mMask);            
            CvInvoke.Dilate(mMask, mDilatedMask, null, _dilateAnchor, 6, BorderType.Default, _dilateBorder);

            VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint();

            CvInvoke.FindContours(mDilatedMask, contours, mHierarchy, RetrType.External, ChainApproxMethod.ChainApproxSimple);

            // Find max contour area
            double maxArea = 0;
            VectorOfPoint curContour;
            for (int i = 0; i < contours.Size; i++)
            {
                curContour = contours[i];
                double area = CvInvoke.ContourArea(curContour);
                if (area > maxArea)
                    maxArea = area;
            }            

            // Filter contours by area and resize to fit the original image size
            mContours.Clear();

            Rectangle contourBBox;
            double contourArea;
            VectorOfPoint newContour;
            for (int i = 0; i < contours.Size; i++)
            {
                curContour = contours[i];
                contourArea = CvInvoke.ContourArea(curContour);
                if (contourArea > mMinContourArea * maxArea)
                {

                    //Mat expander = new Mat(1, curContour.Size, DepthType.Cv8U, 1);
                    //expander.SetTo(new MCvScalar(4));
                    //CvInvoke.Multiply(curContour, expander, curContour);
                    //expander.Dispose();
                    var newPoints = new Point[curContour.Size];
                    for (int jj = 0; jj < curContour.Size; jj++)
                    {
                        newPoints[jj] = new Point(2*curContour[jj].X, 2*curContour[jj].Y);
                    }
                    newContour = new VectorOfPoint(newPoints);                    
                    mContours.Push(newContour);

                    contourBBox = CvInvoke.BoundingRectangle(newContour);
                    Matches.Add(new MotionDetectionMatch(contourBBox, contourArea));
                }
            }

            contours.Dispose();
        }

        public void DrawMatchedBoxes(Mat image)
        {
            foreach (MotionDetectionMatch match in Matches)
            {
                CvInvoke.Rectangle(image, match.BoundingBox, Options.BoxColor, Options.BoxThickness);
            }
        }

    }
}
