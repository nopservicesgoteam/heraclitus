﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hearclitus.ImageProcessing
{
    public class MotionDetectionMatch
    {
        public Rectangle BoundingBox;
        public double Area;

        public MotionDetectionMatch(Rectangle boundingBox, double area)
        {
            this.BoundingBox = boundingBox;
            this.Area = area;
        }
    }
}
