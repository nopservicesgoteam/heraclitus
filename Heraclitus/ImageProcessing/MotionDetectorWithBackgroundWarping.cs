﻿using Emgu.CV;
using Emgu.CV.Features2D;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Emgu.CV.XFeatures2D;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hearclitus.ImageProcessing
{

    public class MotionDetectorWithBackgroundWarpingOptions
    {
        public int MaxWith = 960;
        public int FeaturesDetectorThreshold = 85;
        public int MotionDetectorThreshold = 128;
        public bool EstimateMovingBackground = true;

        public MotionDetectorWithBackgroundWarpingOptions()
        {
            this.MaxWith = 960;
            this.FeaturesDetectorThreshold = 85;
            this.MotionDetectorThreshold = 128;
        }

        public MotionDetectorWithBackgroundWarpingOptions(int maxWidth, int featuresDetectorThreshold, int motionDetectorThreshold)
        {
            this.MaxWith = maxWidth;
            this.FeaturesDetectorThreshold = featuresDetectorThreshold;
            this.MotionDetectorThreshold = motionDetectorThreshold;
        }

        public static MotionDetectorWithBackgroundWarpingOptions GetOptionsFor1920()
        {
            MotionDetectorWithBackgroundWarpingOptions options = new MotionDetectorWithBackgroundWarpingOptions(
                1920,
                105,
                192
                );
            return options;
        }

        public static MotionDetectorWithBackgroundWarpingOptions GetOptionsFor960()
        {
            MotionDetectorWithBackgroundWarpingOptions options = new MotionDetectorWithBackgroundWarpingOptions(
                960,
                60,
                45
                );
            return options;
        }

        public static MotionDetectorWithBackgroundWarpingOptions GetOptionsFor640()
        {
            MotionDetectorWithBackgroundWarpingOptions options = new MotionDetectorWithBackgroundWarpingOptions(
                640,
                40,
                128
                );
            return options;
        }
    }

    public class MotionDetectorWithBackgroundWarping
    {
        public MotionDetectorWithBackgroundWarpingOptions Options = MotionDetectorWithBackgroundWarpingOptions.GetOptionsFor960();
        public SimpleDifferentialMotionDetector MotionDetector = new SimpleDifferentialMotionDetector();
        public SimpleHueDetector HueDetector = new SimpleHueDetector();

        public Mat CurFrame = null;
        public Mat PrevFrame = null;
        public Mat WarpedPrevFrame = null;

        /*
         * Frame Scaling Down 
         */
        private bool _shouldScaleDown = false;
        private Size _scaledDownSize;
        private Mat _blurredFrame = null;
        private Size _blurKernelSize;
        private Point _blurAnchorPoint;

        /*
         * Background Motion Estimation
         */
        private FastDetector _fastDetector = null;
        private BriefDescriptorExtractor _descriptor = null;
        

        public MotionDetectorWithBackgroundWarping()
        {
            this.MotionDetector.Options.Threshold = this.Options.MotionDetectorThreshold;
        }

        public void Setup(Mat frame)
        {
            this.MotionDetector.Finish();
            this.MotionDetector.Configure();

            this.HueDetector.Finish();
            this.HueDetector.Configure();

            this.Finish();            

            _shouldScaleDown = (frame.Size.Width > Options.MaxWith);
            if (_shouldScaleDown)
            {
                double scaleF = Options.MaxWith / (double)frame.Size.Width;
                _scaledDownSize = new Size(Options.MaxWith, (int)(scaleF * frame.Size.Height));
                _blurredFrame = new Mat(frame.Size, frame.Depth, frame.NumberOfChannels);

                double kernelSize = Math.Ceiling(1.0 / scaleF);
                if (kernelSize % 2 == 0) kernelSize += 1;
                _blurKernelSize = new Size((int)kernelSize, (int)kernelSize);
                _blurAnchorPoint = new Point((int)Math.Ceiling(kernelSize / 2.0) - 1, (int)Math.Ceiling(kernelSize / 2.0) - 1);
            }
            else
            {
                _scaledDownSize = frame.Size;
                _blurKernelSize = Size.Empty;
                _blurAnchorPoint = Point.Empty;
            }

            _fastDetector = new FastDetector(Options.FeaturesDetectorThreshold);
            _descriptor = new BriefDescriptorExtractor();

            WarpedPrevFrame = new Mat(_scaledDownSize, frame.Depth, frame.NumberOfChannels);
        }

        private object _fastDetectorLock = new object();
        public void SetFeaturesDetectorThreshold(int threshold)
        {
            lock (_fastDetectorLock)
            {
                if (_fastDetector != null)
                {
                    _fastDetector.Dispose();
                    _fastDetector = null;
                }
                Options.FeaturesDetectorThreshold = threshold;
                _fastDetector = new FastDetector(Options.FeaturesDetectorThreshold);
            }
        }

        public void SetMotionDetectorThreshold(int value)
        {
            this.MotionDetector.Options.Threshold = value;
        }

        public void SetMotionDetectorDilateFactor(int value)
        {
            this.MotionDetector.Options.DilateFactor = value;
        }

        public void SetMotionDetectorErodeFactor(int value)
        {
            this.MotionDetector.Options.ErodeFactor= value;
        }

        public void Finish()
        {
            //dispose old stuff
            if (_blurredFrame != null)
            {
                _blurredFrame.Dispose();
                _blurredFrame = null;
            }

            if (_fastDetector != null)
            {
                _fastDetector.Dispose();
                _fastDetector = null;
            }

            if (_descriptor != null)
            {
                _descriptor.Dispose();
                _descriptor = null;
            }

            if (WarpedPrevFrame != null)
            {
                WarpedPrevFrame.Dispose();
                WarpedPrevFrame = null;
            }

            if (PrevFrame != null)
            {
                PrevFrame.Dispose();
                PrevFrame = null;
            }

            if (CurFrame != null)
            {
                CurFrame.Dispose();
                CurFrame = null;
            }

        }

        public void Reset()
        {
            if (PrevFrame != null) PrevFrame.Dispose();
            PrevFrame = null;

            if (CurFrame != null) CurFrame.Dispose();
            CurFrame = null;
        }

        public void Feed(Mat frame)
        {
            if (frame.Width == 0 || frame.Height == 0) return;

            if (PrevFrame != null) PrevFrame.Dispose();
            PrevFrame = CurFrame;            

            if (_shouldScaleDown)
            {
                CurFrame = new Mat(_scaledDownSize, frame.Depth, frame.NumberOfChannels);

                CvInvoke.Blur(frame, _blurredFrame, _blurKernelSize, _blurAnchorPoint);
                CvInvoke.Resize(_blurredFrame, CurFrame, _scaledDownSize);
            }else
            {
                CurFrame = frame.Clone();
            }            

            if (HueDetector.Options.EnableHueDetection)
            {
                HueDetector.Compute(CurFrame);
            }
            else
            {
                //wait for second frame
                if (PrevFrame == null) return;

                Mat homography = null;

                if (Options.EstimateMovingBackground)
                {

                    VectorOfKeyPoint _prevFrameKeyPoints = new VectorOfKeyPoint();
                    VectorOfKeyPoint _curFrameKeyPoints = new VectorOfKeyPoint();

                    lock (_fastDetectorLock)
                    {
                        _fastDetector.DetectRaw(PrevFrame, _prevFrameKeyPoints);
                        _fastDetector.DetectRaw(CurFrame, _curFrameKeyPoints);
                    }

                    Mat _prevFrameDescriptors = new Mat();
                    Mat _curFrameDescriptors = new Mat();

                    _descriptor.Compute(PrevFrame, _prevFrameKeyPoints, _prevFrameDescriptors);
                    _descriptor.Compute(CurFrame, _curFrameKeyPoints, _curFrameDescriptors);

                    if (false)
                    {
                        //Bgr featuresColor = new Bgr(0, 0, 255);
                        //Features2DToolbox.DrawKeypoints(_prevFrame, modelKeyPoints, out1, featuresColor);
                        //Features2DToolbox.DrawKeypoints(_curFrame, observedKeyPoints, out2, featuresColor);
                    }

                    //Do Matching
                    Mat mask;
                    int k = 2;
                    double uniquenessThreshold = 0.8;

                    BFMatcher matcher = new BFMatcher(DistanceType.L2);
                    matcher.Add(_prevFrameDescriptors);

                    VectorOfVectorOfDMatch matches = new VectorOfVectorOfDMatch();
                    matcher.KnnMatch(_curFrameDescriptors, matches, k, null);
                    mask = new Mat(matches.Size, 1, Emgu.CV.CvEnum.DepthType.Cv8U, 1);
                    mask.SetTo(new MCvScalar(255));

                    try
                    {
                        Features2DToolbox.VoteForUniqueness(matches, uniquenessThreshold, mask);
                    }
                    catch (Exception)
                    {

                        
                    }

                    matcher.Dispose();


                    int nonZeroCount = CvInvoke.CountNonZero(mask);
                    if (nonZeroCount >= 4)
                    {
                        nonZeroCount = Features2DToolbox.VoteForSizeAndOrientation(_prevFrameKeyPoints, _curFrameKeyPoints, matches, mask, 1.5, 20);
                        if (nonZeroCount >= 4)
                        {
                            homography = Features2DToolbox.GetHomographyMatrixFromMatchedFeatures(_prevFrameKeyPoints, _curFrameKeyPoints, matches, mask, 2);
                        }
                    }

                    matches.Dispose();
                    mask.Dispose();
                    _prevFrameDescriptors.Dispose();
                    _curFrameDescriptors.Dispose();
                    _prevFrameKeyPoints.Dispose();
                    _curFrameKeyPoints.Dispose();

                    //no motion detection result here?
                    //if (homography == null) return;
                }

                if (homography != null)
                {
                    CvInvoke.WarpPerspective(PrevFrame, WarpedPrevFrame, homography, PrevFrame.Size);

                    //Dispose used objects
                    homography.Dispose();

                    MotionDetector.Compute(WarpedPrevFrame, CurFrame);
                }
                else
                {
                    MotionDetector.Compute(PrevFrame, CurFrame);
                }
            }



        }

    }
}
