﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hearclitus.ImageProcessing
{
    public class SimpleDifferentialMotionDetectorOptions
    {
        public int Threshold = 128;
        public int DilateFactor = 10;
        public int ErodeFactor = 2;
        public MCvScalar BoxColor = new MCvScalar(0, 0, 255);
        public int BoxThickness = 2;
        public double MinArea = 1;
        public double MaxArea = 500000;

        public SimpleDifferentialMotionDetectorOptions()
        {

        }
    }
    public class SimpleDifferentialMotionDetector
    {
        public SimpleDifferentialMotionDetectorOptions Options = new SimpleDifferentialMotionDetectorOptions();
        public List<MotionDetectionMatch> Matches = new List<MotionDetectionMatch>();

        private Point _dilateAnchor = new Point(-1, -1);

        private Mat _prevFrameGray = null;
        private Mat _curFrameGray = null;
        private Mat _prevFrameBlur = null;
        private Mat _curFrameBlur = null;
        private Mat _diffFrame = null;
        public Mat _threshPic = null;
        private Mat _dilatePic = null;
        private MCvScalar _dilateBorder = new MCvScalar();
        private VectorOfVectorOfPoint _contours = new VectorOfVectorOfPoint();

        private Size _blurKernelSize = new Size(3, 3);
        private Point _blurAnchorPoint = new Point(1, 1);

        public SimpleDifferentialMotionDetector()
        {

        }

        public void Configure()
        {
            _prevFrameGray = new Mat();
            _curFrameGray = new Mat();
            _prevFrameBlur = new Mat();
            _curFrameBlur = new Mat();
            _diffFrame = new Mat();
            _threshPic = new Mat();
            _dilatePic = new Mat();
        }

        public void Finish()
        {
            if (_prevFrameGray != null)
            {
                _prevFrameGray.Dispose();
                _prevFrameGray = null;
            }

            if (_curFrameGray != null)
            {
                _curFrameGray.Dispose();
                _curFrameGray = null;
            }

            if (_prevFrameBlur != null)
            {
                _prevFrameBlur.Dispose();
                _prevFrameBlur = null;
            }

            if (_curFrameBlur != null)
            {
                _curFrameBlur.Dispose();
                _curFrameBlur = null;
            }

            if (_diffFrame != null)
            {
                _diffFrame.Dispose();
                _diffFrame = null;
            }

            if (_threshPic != null)
            {
                _threshPic.Dispose();
                _threshPic = null;
            }

            if (_dilatePic != null)
            {
                _dilatePic.Dispose();
                _dilatePic = null;
            }
        }

        public void Compute(Mat prevFrame, Mat curFrame)
        {
            Matches.Clear();

            //Grayscale first
            CvInvoke.CvtColor(prevFrame, _prevFrameGray, ColorConversion.Bgr2Gray);
            CvInvoke.CvtColor(curFrame, _curFrameGray, ColorConversion.Bgr2Gray);

            //Blur then
            CvInvoke.Blur(_prevFrameGray, _prevFrameBlur, _blurKernelSize, _blurAnchorPoint);
            CvInvoke.Blur(_curFrameGray, _curFrameBlur, _blurKernelSize, _blurAnchorPoint);

            CvInvoke.AbsDiff(_curFrameBlur, _prevFrameBlur, _diffFrame);
            
            CvInvoke.Threshold(_diffFrame, _threshPic, Options.Threshold, 255, ThresholdType.Binary);
                                    
            if (Options.DilateFactor > 0)
            {
                CvInvoke.Dilate(_threshPic, _dilatePic, null, _dilateAnchor, (2 * _threshPic.Width * Options.DilateFactor) / 640, BorderType.Default, _dilateBorder);
            }            
            if (Options.ErodeFactor > 0)
            {
                CvInvoke.Erode(_dilatePic, _dilatePic, null, _dilateAnchor, (2 * _threshPic.Width * Options.ErodeFactor) / 640, BorderType.Default, _dilateBorder);
            }            



            _contours.Clear();
            CvInvoke.FindContours(_dilatePic, _contours, null, RetrType.External, ChainApproxMethod.ChainApproxSimple);
            VectorOfPoint curContour;
            Rectangle contourBBox;            
            double contourArea;
            for (int i = 0; i < _contours.Size; i++)
            {
                curContour = _contours[i];
                contourArea = CvInvoke.ContourArea(curContour);
                if (contourArea >= Options.MinArea && contourArea <= Options.MaxArea)
                {
                    contourBBox = CvInvoke.BoundingRectangle(curContour);
                    Matches.Add(new MotionDetectionMatch(contourBBox, contourArea));                    
                }
            }
        }

        public void DrawMatchedBoxes(Mat image)
        {
            foreach (MotionDetectionMatch match in Matches)
            {
                CvInvoke.Rectangle(image, match.BoundingBox, Options.BoxColor, Options.BoxThickness);
            }            
        }
    }
}
