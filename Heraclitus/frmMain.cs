﻿using DirectShowLib;
using Emgu.CV;
using Emgu.CV.Structure;
using Hearclitus.EmguExtensions;
using Hearclitus.VideoPlayer;
using Heraclitus.Metadata;
using Heraclitus.Models;
using Heraclitus.Properties;
using HeraclitusMapLib.Forms;
using HeraclitusMapLib.Map;
using HeraclitusMapLib.Map.Google;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Heraclitus
{
    public partial class frmMain : Form
    {
        ReaderBuffer readBuffer;
        ViewUpdater viewUpdater;
        FlightMetadata flightMetadata;

        //private VideoDevice[] SystemCams = null;

        private int _defaultFeaturesThreshold = 0;
        private int _defaultMotionThreshold = 0;
        private int _defaultMotionDilateFactor = 0;
        private int _defaultMotionErodeFactor = 0;

        public frmMain()
        {
            InitializeComponent();

            Assembly ass = System.Reflection.Assembly.GetExecutingAssembly();
            AssemblyName assname = ass.GetName();

            this.Text = String.Format("{0} v{1}.{2}.{3}r{4}", assname.Name, assname.Version.Major, assname.Version.Minor, assname.Version.Build, assname.Version.Revision);

            mapCtl1.MapType = Settings.Default.ActiveMapType;
            mapCtl1.MapCacheLocalPath = Settings.Default.MapCacheLocalPath;

            this.populateSystemCameras();

            this.flightMetadata = new FlightMetadata(this.mapCtl1, this.pictureBoxFlightPath);
            this.flightMetadata.LocationChanged += FlightMetadata_LocationChanged;

            this.readBuffer = new ReaderBuffer();
            this.viewUpdater = new ViewUpdater(this.readBuffer, this.trackBar1, this.pictureBox1, this.flightMetadata);
            this.viewUpdater.Start();
        }

        private void FlightMetadata_LocationChanged(object sender, FlightMetadata.LocationArgs e)
        {
            this.mapCtl1.CenterCoordinate = e.Coordinate;
        }

        private void populateSystemCameras()
        {
            toolStripComboBoxSystemCameras.Items.Clear();
            toolStripComboBoxSystemCameras.Items.Add("(select a system camera)");
            toolStripComboBoxSystemCameras.SelectedIndex = 0;

            DsDevice[] _SystemCamereas = DsDevice.GetDevicesOfCat(FilterCategory.VideoInputDevice);            
            //SystemCams = new VideoDevice[_SystemCamereas.Length];
            for (int i = 0; i < _SystemCamereas.Length; i++)
            {
                VideoDevice cam = new VideoDevice(i, _SystemCamereas[i].Name, _SystemCamereas[i].ClassID);
                toolStripComboBoxSystemCameras.Items.Add(cam);
                if (cam.ID == Settings.Default.SelectedCameraID)
                {
                    toolStripComboBoxSystemCameras.SelectedItem = cam;
                }
            }
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            checkBoxUseBackgroundEstimation.Checked = this.readBuffer.MotionDetector.Options.EstimateMovingBackground;
            numFeaturesDetectorThreshold.Value = this.readBuffer.MotionDetector.Options.FeaturesDetectorThreshold;
            numMotionDetectorThreshold.Value = this.readBuffer.MotionDetector.MotionDetector.Options.Threshold;
            numMotionDetectorDilateFactor.Value = this.readBuffer.MotionDetector.MotionDetector.Options.DilateFactor;
            numMotionDetectorErodeFactor.Value = this.readBuffer.MotionDetector.MotionDetector.Options.ErodeFactor;

            checkBoxEnableColorDetection.Checked = this.readBuffer.MotionDetector.HueDetector.Options.EnableHueDetection;
            panelSelectedColor.BackColor = this.readBuffer.MotionDetector.HueDetector.GetSelectedColor();

            _defaultFeaturesThreshold = (int)numFeaturesDetectorThreshold.Value;
            _defaultMotionThreshold = (int)numMotionDetectorThreshold.Value;
            _defaultMotionDilateFactor = (int)numMotionDetectorDilateFactor.Value;
            _defaultMotionErodeFactor = (int)numMotionDetectorErodeFactor.Value;

            labelFeaturesThreshold.Text = string.Format("Features Threshold [{0}]:", _defaultFeaturesThreshold);
            labelMotionThreshold.Text = string.Format("Motion Threshold [{0}]:", _defaultMotionThreshold);
            labelMotionDetectorDilateFactor.Text = string.Format("Dilate Factor [{0}]:", _defaultMotionDilateFactor);
            labelMotionDetectorErodeFactor.Text = string.Format("Erode Factor [{0}]:", _defaultMotionErodeFactor);
        }


        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.mapCtl1.ControlClosing();
            this.viewUpdater.Stop(true);
            this.readBuffer.Stop(true);            
        }

        private void frmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }


        /*
        private void updateFramesProc()
        {
            int prevFrameNo = -1;
            bool updateFrame = false;
            Bitmap newFrame = null;
            int progValue = 0;
            int emptyLoops = 0;

            while (mRunning)
            {
                //Console.WriteLine("UpdateFrames");
                updateFrame = false;
                emptyLoops++;
                {
                    if (mRunning)
                    {
                        if (mVideoIsPlaying)
                        {
                            if (prevFrameNo != mVideoCurrentFrame)
                            {
                                Console.WriteLine(" * Updating Frame #{0} [{1}]", mVideoCurrentFrame, emptyLoops);
                                emptyLoops = 0;

                                if (mForceMoveToFrameNo != null)
                                {
                                    mVideoCurrentFrame = mForceMoveToFrameNo.Value;
                                    mForceMoveToFrameNo = null;
                                    mVideoCapture.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.PosFrames, mVideoCurrentFrame);
                                }

                                mVideoCapture.Read(mCurrentFrame);
                                mVideoCurrentFrame = (int)mVideoCapture.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.PosFrames);
                                progValue = mVideoCurrentFrame;

                                updateFrame = true;
                                newFrame = mCurrentFrame.Bitmap;

                                prevFrameNo = mVideoCurrentFrame;
                            }
                        }
                        else
                        {
                            if (pictureBox1.Image != null)
                            {
                                progValue = 0;
                                updateFrame = true;
                                newFrame = null;
                            }
                        }

                        if (updateFrame)
                        {
                            
                            this.Invoke(new Action(() => {
                                if (mTrackBarUpdate)
                                {
                                    trackBar1.Value = progValue;
                                }                                
                                pictureBox1.Image = newFrame;
                            }));
                        }
                    }
                }

                Thread.Sleep(1);

            }
        }
        */

        private void mnuClose_Click(object sender, EventArgs e)
        {
            this.readBuffer.Stop(true);
            //stopVideo();
            /*
            String image_file = "C:\\Users\\Public\\Pictures\\Sample Pictures\\Chrysanthemum.jpg";

            Image<Bgr, Byte> cvImage = new Image<Bgr, Byte>(image_file);
            Image<Gray, Byte> grayImage = cvImage.Convert<Gray, Byte>();
            pictureBox1.Image = grayImage.ToBitmap();
            */
        }

        private void cameraToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (toolStripComboBoxSystemCameras.SelectedItem != null)
            {
                if (toolStripComboBoxSystemCameras.SelectedItem is VideoDevice videoDevice)
                {
                    Settings.Default.SelectedCameraID = videoDevice.ID;
                    Settings.Default.Save();
                    bool success = this.readBuffer.Start(String.Format("videodevice://{0}", videoDevice.ID));
                    if (!success)
                    {
                        MessageBox.Show("Failed to open video device", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Please select a system camera from the list", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }            
        }

        private void streamingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool success = this.readBuffer.Start(Settings.Default.StreamingURL);
            if (!success)
            {
                MessageBox.Show("Failed to connect to streaming server", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void mnuOpen_Click(object sender, EventArgs e)
        {
            openFileDialog1.FileName = null;
            DialogResult res = openFileDialog1.ShowDialog();

            if (res != DialogResult.OK) return;

            if (!System.IO.File.Exists(openFileDialog1.FileName)) return;

            openFileDialog1.InitialDirectory = Path.GetDirectoryName(openFileDialog1.FileName);

            bool success = this.readBuffer.Start(openFileDialog1.FileName);

            if (!success)
            {
                MessageBox.Show("Failed to open video file", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            /*
            this.Cursor = Cursors.WaitCursor;
            this.Refresh();

            if (mCurrentFrame != null)
            {
                mCurrentFrame.Dispose();
                mCurrentFrame = null;
            }

            if (mVideoCapture != null)
            {
                mVideoCapture.Dispose();
                mVideoCapture = null;
            }

            mVideoCapture = new VideoCapture(openFileDialog1.FileName);
            mVideoTotalFrames = Convert.ToInt32(mVideoCapture.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameCount));
            mFPS = Convert.ToInt32(mVideoCapture.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.Fps ));
            mFrameWaitMillis = Convert.ToInt32(1000.0 / mFPS);
            mVideoIsPlaying = true;
            mCurrentFrame = new Mat();
            mVideoCurrentFrame = 0;

            trackBar1.Minimum = 0;
            trackBar1.Maximum = mVideoTotalFrames;
            trackBar1.Value = 0;

            this.Cursor = Cursors.Default;

            PlayVideo();        
            */
        }


        private void trackBar1_MouseDown(object sender, MouseEventArgs e)
        {
            this.viewUpdater.enableTrackBarUpdate = false;
        }

        private void trackBar1_MouseUp(object sender, MouseEventArgs e)
        {
            this.readBuffer.GoToFrameNumber(trackBar1.Value);
            this.viewUpdater.enableTrackBarUpdate = true;
        }

        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            
        }

        private void checkBoxUseBackgroundEstimation_CheckedChanged(object sender, EventArgs e)
        {
            this.readBuffer.MotionDetector.Options.EstimateMovingBackground = checkBoxUseBackgroundEstimation.Checked;
        }

        private void numFeaturesDetectorThreshold_ValueChanged(object sender, EventArgs e)
        {
            this.readBuffer.MotionDetector.SetFeaturesDetectorThreshold((int)numFeaturesDetectorThreshold.Value);
        }

        private void numMotionDetectorThreshold_ValueChanged(object sender, EventArgs e)
        {
            this.readBuffer.MotionDetector.SetMotionDetectorThreshold((int)numMotionDetectorThreshold.Value);
        }

        private void numMotionDetectorDilateFactor_ValueChanged(object sender, EventArgs e)
        {
            this.readBuffer.MotionDetector.SetMotionDetectorDilateFactor((int)numMotionDetectorDilateFactor.Value);
        }

        private void numMotionDetectorErodeFactor_ValueChanged(object sender, EventArgs e)
        {
            this.readBuffer.MotionDetector.SetMotionDetectorErodeFactor((int)numMotionDetectorErodeFactor.Value);
        }

        private void labelMotionThreshold_Click(object sender, EventArgs e)
        {
            this.readBuffer.MotionDetector.SetMotionDetectorThreshold(_defaultMotionThreshold);
            numMotionDetectorThreshold.Value = this.readBuffer.MotionDetector.MotionDetector.Options.Threshold;
        }

        private void labelMotionDetectorDilateFactor_Click(object sender, EventArgs e)
        {
            this.readBuffer.MotionDetector.SetMotionDetectorDilateFactor(_defaultMotionDilateFactor);
            numMotionDetectorDilateFactor.Value = this.readBuffer.MotionDetector.MotionDetector.Options.DilateFactor;
        }

        private void labelMotionDetectorErodeFactor_Click(object sender, EventArgs e)
        {
            this.readBuffer.MotionDetector.SetMotionDetectorErodeFactor(_defaultMotionErodeFactor);
            numMotionDetectorErodeFactor.Value = this.readBuffer.MotionDetector.MotionDetector.Options.ErodeFactor;
        }

        private void labelFeaturesThreshold_Click(object sender, EventArgs e)
        {
            this.readBuffer.MotionDetector.SetFeaturesDetectorThreshold(_defaultFeaturesThreshold);
            numFeaturesDetectorThreshold.Value = this.readBuffer.MotionDetector.Options.FeaturesDetectorThreshold;
        }

        private void buttonLoadFlightData_Click(object sender, EventArgs e)
        {
            openFileDialogCsvMetadata.FileName = null;
            DialogResult res = openFileDialogCsvMetadata.ShowDialog();

            if (res != DialogResult.OK) return;

            if (!System.IO.File.Exists(openFileDialogCsvMetadata.FileName)) return;

            openFileDialogCsvMetadata.InitialDirectory = Path.GetDirectoryName(openFileDialogCsvMetadata.FileName);

            string errmsg = this.flightMetadata.Load(openFileDialogCsvMetadata.FileName);

            if (errmsg != null)
            {
                MessageBox.Show(string.Format("Error loading flight metadata file:\r\n{0}", errmsg), "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            this.labelFlightDataFile.Text = Path.GetFileName(openFileDialogCsvMetadata.FileName);
        }

        private void buttonClearFlightData_Click(object sender, EventArgs e)
        {
            flightMetadata.ClearData();
            this.labelFlightDataFile.Text = string.Empty;
        }

        private void trackBar2_Scroll(object sender, EventArgs e)
        {
            double relativeOffset = trackBar2.Value / (double)trackBar2.Maximum;
            flightMetadata.SetRelativeOffset(relativeOffset);
        }
             
        private void buttonStop_Click(object sender, EventArgs e)
        {
            this.readBuffer.Stop(true);
        }

        private void buttonPause_Click(object sender, EventArgs e)
        {
            this.readBuffer.Pause();
        }

        private void buttonPlay_Click(object sender, EventArgs e)
        {
            this.readBuffer.Unpause();
        }

        private void checkBoxEnableColorDetection_CheckedChanged(object sender, EventArgs e)
        {
            this.readBuffer.MotionDetector.HueDetector.Options.EnableHueDetection = checkBoxEnableColorDetection.Checked;
        }

        private void panelSelectedColor_Click(object sender, EventArgs e)
        {
            int[] cc = colorDetectionDialog.CustomColors;
            colorDetectionDialog.Color = this.readBuffer.MotionDetector.HueDetector.GetSelectedColor();
            colorDetectionDialog.CustomColors = new int[] {
                ColorTranslator.ToOle(colorDetectionDialog.Color)
            };
            DialogResult res = colorDetectionDialog.ShowDialog();

            if (res == DialogResult.OK)
            {
                this.readBuffer.MotionDetector.HueDetector.SetHsvColor(colorDetectionDialog.Color);
                panelSelectedColor.BackColor = this.readBuffer.MotionDetector.HueDetector.GetSelectedColor();
            }
        }

        private void buttonSetColorRoof_Click(object sender, EventArgs e)
        {
            this.readBuffer.MotionDetector.HueDetector.SetHsvColor(Color.FromArgb(249, 175, 177));
            panelSelectedColor.BackColor = this.readBuffer.MotionDetector.HueDetector.GetSelectedColor();
        }

        private void buttonSetColorTractor_Click(object sender, EventArgs e)
        {
            this.readBuffer.MotionDetector.HueDetector.SetHsvColor(Color.FromArgb(167, 234, 246));
            panelSelectedColor.BackColor = this.readBuffer.MotionDetector.HueDetector.GetSelectedColor();
        }

        private void pictureBox1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (pictureBox1.Image != null)
            {
                Bitmap bmp = (Bitmap) pictureBox1.Image;

                int fw = bmp.Width;
                int fh = bmp.Height;
                double fR = (double)fw / (double)fh;

                int dw = pictureBox1.Width;
                int dh = pictureBox1.Height;
                double dR = (double)dw / (double)dh;

                double d_pic_width = dw;
                double d_pic_height = dh;

                int offX = 0;
                int offY = 0;
                if (fR >= dR)
                {
                    //frame wider than display
                    //blank areas on top/bottom
                    d_pic_height = dw / fR;
                    offY = (int)Math.Round( (dh - d_pic_height) / 2.0 );                    
                }
                else
                {
                    //frame taller than display
                    //blank areas on left/right
                    d_pic_width = dh * fR;
                    offX = (int)Math.Round((dw - d_pic_width) / 2.0);
                }

                int clickX = e.X - offX;
                int clickY = e.Y - offY;

                if (clickX >= 0 && clickX < d_pic_width-1 && clickY >= 0 && clickY < d_pic_height)
                {
                    double fX = (double)clickX / (d_pic_width - 1);
                    double fY = (double)clickY / (d_pic_height - 1);

                    int frameX = (int)Math.Round(fX * (bmp.Width - 1));
                    int frameY = (int)Math.Round(fY * (bmp.Height - 1));
                    
                    Color c = bmp.GetPixel(frameX, frameY);
                    this.readBuffer.MotionDetector.HueDetector.SetHsvColor(c);
                    panelSelectedColor.BackColor = this.readBuffer.MotionDetector.HueDetector.GetSelectedColor();

                    Console.WriteLine(String.Format("CLICK {0}, {1}: ({2}, {3}, {4})", frameX, frameY, c.R, c.G, c.B));
                }

            }
            //this.readBuffer.MotionDetector.HueDetector.SetHsvColorFromUserClick();
        }

        private void pictureBoxLogoAnax_MouseClick(object sender, MouseEventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.anax-group.com");
        }

        private void pictureBoxLogoNop_MouseClick(object sender, MouseEventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.nopservices.com");
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormAbout dlg = new FormAbout();
            dlg.ShowDialog();
        }

        private void mapCtl1_LevelValueChanged(object sender, HeraclitusMapLib.Controls.ButtonPanelCtl.LevelValueArgs e)
        {
            mapSimpleButtonPanel1.Level = e.Level;
        }

        private void mapCtl1_MapTypeIndexChanged(object sender, HeraclitusMapLib.Controls.ButtonPanelCtl.MapTypeIndexArgs e)
        {
            mapSimpleButtonPanel1.MapTypeIndex = e.MapTypeIndex;
        }

        private void mapSimpleButtonPanel1_CenterMapClicked(object sender, EventArgs e)
        {
            mapCtl1.SetCenterMap();
        }

        private void mapSimpleButtonPanel1_LevelValueChanged(object sender, HeraclitusMapLib.Controls.MapSimpleButtonPanel.LevelValueArgs e)
        {
            mapCtl1.Level = e.Level;
        }

        private void mapSimpleButtonPanel1_MapTypeIndexChanged(object sender, HeraclitusMapLib.Controls.MapSimpleButtonPanel.MapTypeIndexArgs e)
        {
            GoogleMapType mapType = (GoogleMapType)e.MapTypeIndex;
            Settings.Default.ActiveMapType = mapType;
            Settings.Default.Save();
            mapCtl1.MapType = mapType;
        }

        private void mapSimpleButtonPanel1_PrintMapClicked(object sender, EventArgs e)
        {
            if (printDialog1.ShowDialog() == DialogResult.OK)
                printDocument1.Print();
        }

        private void mapSimpleButtonPanel1_SaveAllMapClicked(object sender, EventArgs e)
        {
            CoordinateRectangle mapView = mapCtl1.CoordinateView;
            FrmMapDownloader.DownloadMap(mapCtl1.MapType, mapView, mapCtl1.MapCacheLocalPath);
        }

        private void mapSimpleButtonPanel1_SaveMapAsImageClicked(object sender, EventArgs e)
        {
            CoordinateRectangle mapView = mapCtl1.CoordinateView;
            FrmMapDownloader.SaveMapAsImage(mapCtl1.MapType, mapView, mapCtl1.PiFormat, mapCtl1.Level, mapCtl1.MapCacheLocalPath);
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            e.HasMorePages = false;
            e.Graphics.InterpolationMode = InterpolationMode.HighQualityBilinear;
            try
            {
                var imageMap = mapCtl1.GetMapImageForPrint();
                var printSize = e.PageBounds.Size;
                var k1 = (double)imageMap.Width / printSize.Width;
                var k2 = (double)imageMap.Height / printSize.Height;
                var k = (k1 > k2) ? k1 : k2;
                var newSize = new Size((int)(imageMap.Size.Width / k), (int)(imageMap.Size.Height / k));

                var screnCenter = new Point(printSize.Width / 2, printSize.Height / 2);
                var mapCenter = new Point(newSize.Width / 2, newSize.Height / 2);
                var shift = new Size(screnCenter.X - mapCenter.X, screnCenter.Y - mapCenter.Y);
                var p = new Point(0, 0) + shift;

                var rectangle = new Rectangle(p, newSize);
                e.Graphics.DrawImage(imageMap, rectangle);
            }
            catch (Exception ex)
            {
                //do nothing
                System.Diagnostics.Trace.WriteLine(ex.Message);
            }
        }

    }

}

