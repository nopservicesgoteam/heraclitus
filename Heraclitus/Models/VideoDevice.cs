﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heraclitus.Models
{
    public class VideoDevice
    {
        public string Name;
        public int ID;
        public Guid Identifier;

        public VideoDevice(int aID, string aName, Guid aIdentifier = new Guid())
        {
            ID = aID;
            Name = aName;
            Identifier = aIdentifier;
        }

        /// <summary>
        /// Represent the Device as a String
        /// </summary>
        /// <returns>The string representation of this color</returns>
        public override string ToString()
        {
            return String.Format("{0}", Name);
        }

    }
}
