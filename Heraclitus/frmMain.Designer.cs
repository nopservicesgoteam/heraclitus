﻿namespace Heraclitus
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.numFeaturesDetectorThreshold = new System.Windows.Forms.NumericUpDown();
            this.labelFeaturesThreshold = new System.Windows.Forms.Label();
            this.checkBoxUseBackgroundEstimation = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.numMotionDetectorErodeFactor = new System.Windows.Forms.NumericUpDown();
            this.labelMotionDetectorErodeFactor = new System.Windows.Forms.Label();
            this.numMotionDetectorDilateFactor = new System.Windows.Forms.NumericUpDown();
            this.labelMotionDetectorDilateFactor = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.numMotionDetectorThreshold = new System.Windows.Forms.NumericUpDown();
            this.labelMotionThreshold = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.mapCtl1 = new HeraclitusMapLib.Controls.MapCtl();
            this.pictureBoxFlightPath = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.trackBar2 = new System.Windows.Forms.TrackBar();
            this.labelFlightDataFile = new System.Windows.Forms.Label();
            this.buttonLoadFlightData = new System.Windows.Forms.Button();
            this.buttonClearFlightData = new System.Windows.Forms.Button();
            this.mapSimpleButtonPanel1 = new HeraclitusMapLib.Controls.MapSimpleButtonPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonPlay = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.buttonPause = new System.Windows.Forms.Button();
            this.buttonStop = new System.Windows.Forms.Button();
            this.pictureBoxLogoAnax = new System.Windows.Forms.PictureBox();
            this.pictureBoxLogoNop = new System.Windows.Forms.PictureBox();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.buttonSetColorTractor = new System.Windows.Forms.Button();
            this.buttonSetColorRoof = new System.Windows.Forms.Button();
            this.panelSelectedColor = new System.Windows.Forms.Panel();
            this.checkBoxEnableColorDetection = new System.Windows.Forms.CheckBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.mnuClose = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.streamingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cameraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBoxSystemCameras = new System.Windows.Forms.ToolStripComboBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.openFileDialogCsvMetadata = new System.Windows.Forms.OpenFileDialog();
            this.colorDetectionDialog = new System.Windows.Forms.ColorDialog();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numFeaturesDetectorThreshold)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numMotionDetectorErodeFactor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMotionDetectorDilateFactor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMotionDetectorThreshold)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFlightPath)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).BeginInit();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogoAnax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogoNop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 450F));
            this.tableLayoutPanel1.Controls.Add(this.pictureBox1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.groupBox3, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.groupBox4, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 40);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1264, 641);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Black;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Location = new System.Drawing.Point(153, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(658, 535);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDoubleClick);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(144, 535);
            this.panel1.TabIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.numFeaturesDetectorThreshold);
            this.groupBox1.Controls.Add(this.labelFeaturesThreshold);
            this.groupBox1.Controls.Add(this.checkBoxUseBackgroundEstimation);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 302);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(10);
            this.groupBox1.Size = new System.Drawing.Size(144, 232);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Background Estimation";
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label3.Location = new System.Drawing.Point(10, 170);
            this.label3.Name = "label3";
            this.label3.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.label3.Size = new System.Drawing.Size(124, 54);
            this.label3.TabIndex = 4;
            this.label3.Text = "The lower the threshold the more acurate (and slower) the estimation is.";
            // 
            // numFeaturesDetectorThreshold
            // 
            this.numFeaturesDetectorThreshold.Dock = System.Windows.Forms.DockStyle.Top;
            this.numFeaturesDetectorThreshold.Location = new System.Drawing.Point(10, 150);
            this.numFeaturesDetectorThreshold.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.numFeaturesDetectorThreshold.Minimum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numFeaturesDetectorThreshold.Name = "numFeaturesDetectorThreshold";
            this.numFeaturesDetectorThreshold.Size = new System.Drawing.Size(124, 20);
            this.numFeaturesDetectorThreshold.TabIndex = 3;
            this.numFeaturesDetectorThreshold.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
            this.numFeaturesDetectorThreshold.ValueChanged += new System.EventHandler(this.numFeaturesDetectorThreshold_ValueChanged);
            // 
            // labelFeaturesThreshold
            // 
            this.labelFeaturesThreshold.Cursor = System.Windows.Forms.Cursors.Hand;
            this.labelFeaturesThreshold.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelFeaturesThreshold.Location = new System.Drawing.Point(10, 127);
            this.labelFeaturesThreshold.Name = "labelFeaturesThreshold";
            this.labelFeaturesThreshold.Padding = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.labelFeaturesThreshold.Size = new System.Drawing.Size(124, 23);
            this.labelFeaturesThreshold.TabIndex = 2;
            this.labelFeaturesThreshold.Text = "Features Threshold:";
            this.labelFeaturesThreshold.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.labelFeaturesThreshold.Click += new System.EventHandler(this.labelFeaturesThreshold_Click);
            // 
            // checkBoxUseBackgroundEstimation
            // 
            this.checkBoxUseBackgroundEstimation.Dock = System.Windows.Forms.DockStyle.Top;
            this.checkBoxUseBackgroundEstimation.Location = new System.Drawing.Point(10, 97);
            this.checkBoxUseBackgroundEstimation.Name = "checkBoxUseBackgroundEstimation";
            this.checkBoxUseBackgroundEstimation.Size = new System.Drawing.Size(124, 30);
            this.checkBoxUseBackgroundEstimation.TabIndex = 0;
            this.checkBoxUseBackgroundEstimation.Text = "Enable";
            this.checkBoxUseBackgroundEstimation.UseVisualStyleBackColor = true;
            this.checkBoxUseBackgroundEstimation.CheckedChanged += new System.EventHandler(this.checkBoxUseBackgroundEstimation_CheckedChanged);
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label1.Location = new System.Drawing.Point(10, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 74);
            this.label1.TabIndex = 1;
            this.label1.Text = "Advanced (slower) algorithm for moving background estimation, used for moving cam" +
    "eras.";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.numMotionDetectorErodeFactor);
            this.groupBox2.Controls.Add(this.labelMotionDetectorErodeFactor);
            this.groupBox2.Controls.Add(this.numMotionDetectorDilateFactor);
            this.groupBox2.Controls.Add(this.labelMotionDetectorDilateFactor);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.numMotionDetectorThreshold);
            this.groupBox2.Controls.Add(this.labelMotionThreshold);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(10);
            this.groupBox2.Size = new System.Drawing.Size(144, 302);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Motion Estimation";
            // 
            // numMotionDetectorErodeFactor
            // 
            this.numMotionDetectorErodeFactor.Dock = System.Windows.Forms.DockStyle.Top;
            this.numMotionDetectorErodeFactor.Location = new System.Drawing.Point(10, 171);
            this.numMotionDetectorErodeFactor.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numMotionDetectorErodeFactor.Name = "numMotionDetectorErodeFactor";
            this.numMotionDetectorErodeFactor.Size = new System.Drawing.Size(124, 20);
            this.numMotionDetectorErodeFactor.TabIndex = 9;
            this.numMotionDetectorErodeFactor.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numMotionDetectorErodeFactor.ValueChanged += new System.EventHandler(this.numMotionDetectorErodeFactor_ValueChanged);
            // 
            // labelMotionDetectorErodeFactor
            // 
            this.labelMotionDetectorErodeFactor.Cursor = System.Windows.Forms.Cursors.Hand;
            this.labelMotionDetectorErodeFactor.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelMotionDetectorErodeFactor.Location = new System.Drawing.Point(10, 149);
            this.labelMotionDetectorErodeFactor.Name = "labelMotionDetectorErodeFactor";
            this.labelMotionDetectorErodeFactor.Padding = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.labelMotionDetectorErodeFactor.Size = new System.Drawing.Size(124, 22);
            this.labelMotionDetectorErodeFactor.TabIndex = 8;
            this.labelMotionDetectorErodeFactor.Text = "Erode Factor:";
            this.labelMotionDetectorErodeFactor.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.labelMotionDetectorErodeFactor.Click += new System.EventHandler(this.labelMotionDetectorErodeFactor_Click);
            // 
            // numMotionDetectorDilateFactor
            // 
            this.numMotionDetectorDilateFactor.Dock = System.Windows.Forms.DockStyle.Top;
            this.numMotionDetectorDilateFactor.Location = new System.Drawing.Point(10, 129);
            this.numMotionDetectorDilateFactor.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numMotionDetectorDilateFactor.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numMotionDetectorDilateFactor.Name = "numMotionDetectorDilateFactor";
            this.numMotionDetectorDilateFactor.Size = new System.Drawing.Size(124, 20);
            this.numMotionDetectorDilateFactor.TabIndex = 7;
            this.numMotionDetectorDilateFactor.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numMotionDetectorDilateFactor.ValueChanged += new System.EventHandler(this.numMotionDetectorDilateFactor_ValueChanged);
            // 
            // labelMotionDetectorDilateFactor
            // 
            this.labelMotionDetectorDilateFactor.Cursor = System.Windows.Forms.Cursors.Hand;
            this.labelMotionDetectorDilateFactor.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelMotionDetectorDilateFactor.Location = new System.Drawing.Point(10, 107);
            this.labelMotionDetectorDilateFactor.Name = "labelMotionDetectorDilateFactor";
            this.labelMotionDetectorDilateFactor.Padding = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.labelMotionDetectorDilateFactor.Size = new System.Drawing.Size(124, 22);
            this.labelMotionDetectorDilateFactor.TabIndex = 6;
            this.labelMotionDetectorDilateFactor.Text = "Dilate Factor:";
            this.labelMotionDetectorDilateFactor.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.labelMotionDetectorDilateFactor.Click += new System.EventHandler(this.labelMotionDetectorDilateFactor_Click);
            // 
            // label5
            // 
            this.label5.Dock = System.Windows.Forms.DockStyle.Top;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label5.Location = new System.Drawing.Point(10, 60);
            this.label5.Name = "label5";
            this.label5.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.label5.Size = new System.Drawing.Size(124, 47);
            this.label5.TabIndex = 5;
            this.label5.Text = "The lower the threshold the more sensitive the motion detection is.";
            // 
            // numMotionDetectorThreshold
            // 
            this.numMotionDetectorThreshold.Dock = System.Windows.Forms.DockStyle.Top;
            this.numMotionDetectorThreshold.Location = new System.Drawing.Point(10, 40);
            this.numMotionDetectorThreshold.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numMotionDetectorThreshold.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numMotionDetectorThreshold.Name = "numMotionDetectorThreshold";
            this.numMotionDetectorThreshold.Size = new System.Drawing.Size(124, 20);
            this.numMotionDetectorThreshold.TabIndex = 4;
            this.numMotionDetectorThreshold.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numMotionDetectorThreshold.ValueChanged += new System.EventHandler(this.numMotionDetectorThreshold_ValueChanged);
            // 
            // labelMotionThreshold
            // 
            this.labelMotionThreshold.Cursor = System.Windows.Forms.Cursors.Hand;
            this.labelMotionThreshold.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelMotionThreshold.Location = new System.Drawing.Point(10, 23);
            this.labelMotionThreshold.Name = "labelMotionThreshold";
            this.labelMotionThreshold.Padding = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.labelMotionThreshold.Size = new System.Drawing.Size(124, 17);
            this.labelMotionThreshold.TabIndex = 3;
            this.labelMotionThreshold.Text = "Motion Threshold:";
            this.labelMotionThreshold.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.labelMotionThreshold.Click += new System.EventHandler(this.labelMotionThreshold_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.mapCtl1);
            this.groupBox3.Controls.Add(this.pictureBoxFlightPath);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.trackBar2);
            this.groupBox3.Controls.Add(this.labelFlightDataFile);
            this.groupBox3.Controls.Add(this.buttonLoadFlightData);
            this.groupBox3.Controls.Add(this.buttonClearFlightData);
            this.groupBox3.Controls.Add(this.mapSimpleButtonPanel1);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(817, 3);
            this.groupBox3.Name = "groupBox3";
            this.tableLayoutPanel1.SetRowSpan(this.groupBox3, 2);
            this.groupBox3.Size = new System.Drawing.Size(444, 635);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Flight Data";
            // 
            // mapCtl1
            // 
            this.mapCtl1.AutoSize = true;
            this.mapCtl1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.mapCtl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.mapCtl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mapCtl1.Level = 14;
            this.mapCtl1.Location = new System.Drawing.Point(3, 54);
            this.mapCtl1.MapCacheLocalPath = null;
            this.mapCtl1.MapType = HeraclitusMapLib.Map.Google.GoogleMapType.StandardRoadmap;
            this.mapCtl1.Name = "mapCtl1";
            this.mapCtl1.Size = new System.Drawing.Size(438, 374);
            this.mapCtl1.TabIndex = 9;
            this.mapCtl1.LevelValueChanged += new System.EventHandler<HeraclitusMapLib.Controls.ButtonPanelCtl.LevelValueArgs>(this.mapCtl1_LevelValueChanged);
            this.mapCtl1.MapTypeIndexChanged += new System.EventHandler<HeraclitusMapLib.Controls.ButtonPanelCtl.MapTypeIndexArgs>(this.mapCtl1_MapTypeIndexChanged);
            // 
            // pictureBoxFlightPath
            // 
            this.pictureBoxFlightPath.BackColor = System.Drawing.Color.FloralWhite;
            this.pictureBoxFlightPath.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBoxFlightPath.Location = new System.Drawing.Point(3, 428);
            this.pictureBoxFlightPath.Name = "pictureBoxFlightPath";
            this.pictureBoxFlightPath.Size = new System.Drawing.Size(438, 68);
            this.pictureBoxFlightPath.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxFlightPath.TabIndex = 2;
            this.pictureBoxFlightPath.TabStop = false;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label2.Location = new System.Drawing.Point(3, 496);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(438, 21);
            this.label2.TabIndex = 7;
            this.label2.Text = "Time Offset:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // trackBar2
            // 
            this.trackBar2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.trackBar2.Location = new System.Drawing.Point(3, 517);
            this.trackBar2.Maximum = 1000;
            this.trackBar2.Name = "trackBar2";
            this.trackBar2.Size = new System.Drawing.Size(438, 45);
            this.trackBar2.TabIndex = 3;
            this.trackBar2.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBar2.Scroll += new System.EventHandler(this.trackBar2_Scroll);
            // 
            // labelFlightDataFile
            // 
            this.labelFlightDataFile.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelFlightDataFile.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelFlightDataFile.Location = new System.Drawing.Point(3, 562);
            this.labelFlightDataFile.Name = "labelFlightDataFile";
            this.labelFlightDataFile.Size = new System.Drawing.Size(438, 24);
            this.labelFlightDataFile.TabIndex = 4;
            this.labelFlightDataFile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // buttonLoadFlightData
            // 
            this.buttonLoadFlightData.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buttonLoadFlightData.Location = new System.Drawing.Point(3, 586);
            this.buttonLoadFlightData.Name = "buttonLoadFlightData";
            this.buttonLoadFlightData.Size = new System.Drawing.Size(438, 23);
            this.buttonLoadFlightData.TabIndex = 5;
            this.buttonLoadFlightData.Text = "Load Flight Data";
            this.buttonLoadFlightData.UseVisualStyleBackColor = true;
            this.buttonLoadFlightData.Click += new System.EventHandler(this.buttonLoadFlightData_Click);
            // 
            // buttonClearFlightData
            // 
            this.buttonClearFlightData.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buttonClearFlightData.Location = new System.Drawing.Point(3, 609);
            this.buttonClearFlightData.Name = "buttonClearFlightData";
            this.buttonClearFlightData.Size = new System.Drawing.Size(438, 23);
            this.buttonClearFlightData.TabIndex = 6;
            this.buttonClearFlightData.Text = "Clear Flight Data";
            this.buttonClearFlightData.UseVisualStyleBackColor = true;
            this.buttonClearFlightData.Click += new System.EventHandler(this.buttonClearFlightData_Click);
            // 
            // mapSimpleButtonPanel1
            // 
            this.mapSimpleButtonPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.mapSimpleButtonPanel1.Level = 14;
            this.mapSimpleButtonPanel1.Location = new System.Drawing.Point(3, 16);
            this.mapSimpleButtonPanel1.MapTypeIndex = -1;
            this.mapSimpleButtonPanel1.MaximumSize = new System.Drawing.Size(20000, 38);
            this.mapSimpleButtonPanel1.MinimumSize = new System.Drawing.Size(325, 38);
            this.mapSimpleButtonPanel1.Name = "mapSimpleButtonPanel1";
            this.mapSimpleButtonPanel1.Size = new System.Drawing.Size(438, 38);
            this.mapSimpleButtonPanel1.TabIndex = 8;
            this.mapSimpleButtonPanel1.LevelValueChanged += new System.EventHandler<HeraclitusMapLib.Controls.MapSimpleButtonPanel.LevelValueArgs>(this.mapSimpleButtonPanel1_LevelValueChanged);
            this.mapSimpleButtonPanel1.MapTypeIndexChanged += new System.EventHandler<HeraclitusMapLib.Controls.MapSimpleButtonPanel.MapTypeIndexArgs>(this.mapSimpleButtonPanel1_MapTypeIndexChanged);
            this.mapSimpleButtonPanel1.CenterMapClicked += new System.EventHandler(this.mapSimpleButtonPanel1_CenterMapClicked);
            this.mapSimpleButtonPanel1.PrintMapClicked += new System.EventHandler(this.mapSimpleButtonPanel1_PrintMapClicked);
            this.mapSimpleButtonPanel1.SaveAllMapClicked += new System.EventHandler(this.mapSimpleButtonPanel1_SaveAllMapClicked);
            this.mapSimpleButtonPanel1.SaveMapAsImageClicked += new System.EventHandler(this.mapSimpleButtonPanel1_SaveMapAsImageClicked);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tableLayoutPanel2);
            this.panel2.Controls.Add(this.trackBar1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(153, 544);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(658, 94);
            this.panel2.TabIndex = 4;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel2.ColumnCount = 5;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 52F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 52F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 52F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.buttonPlay, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.buttonPause, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.buttonStop, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.pictureBoxLogoAnax, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.pictureBoxLogoNop, 4, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 45);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(658, 49);
            this.tableLayoutPanel2.TabIndex = 2;
            // 
            // buttonPlay
            // 
            this.buttonPlay.BackColor = System.Drawing.Color.White;
            this.buttonPlay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonPlay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPlay.ImageKey = "icon_play_2.png";
            this.buttonPlay.ImageList = this.imageList1;
            this.buttonPlay.Location = new System.Drawing.Point(358, 3);
            this.buttonPlay.Name = "buttonPlay";
            this.buttonPlay.Size = new System.Drawing.Size(46, 43);
            this.buttonPlay.TabIndex = 3;
            this.buttonPlay.UseVisualStyleBackColor = false;
            this.buttonPlay.Click += new System.EventHandler(this.buttonPlay_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "icon_play_2.png");
            this.imageList1.Images.SetKeyName(1, "icon_stop_2.png");
            this.imageList1.Images.SetKeyName(2, "icon_pause_2.png");
            // 
            // buttonPause
            // 
            this.buttonPause.BackColor = System.Drawing.Color.White;
            this.buttonPause.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonPause.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPause.ImageKey = "icon_pause_2.png";
            this.buttonPause.ImageList = this.imageList1;
            this.buttonPause.Location = new System.Drawing.Point(306, 3);
            this.buttonPause.Name = "buttonPause";
            this.buttonPause.Size = new System.Drawing.Size(46, 43);
            this.buttonPause.TabIndex = 2;
            this.buttonPause.UseVisualStyleBackColor = false;
            this.buttonPause.Click += new System.EventHandler(this.buttonPause_Click);
            // 
            // buttonStop
            // 
            this.buttonStop.BackColor = System.Drawing.Color.White;
            this.buttonStop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonStop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStop.ImageKey = "icon_stop_2.png";
            this.buttonStop.ImageList = this.imageList1;
            this.buttonStop.Location = new System.Drawing.Point(254, 3);
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Size = new System.Drawing.Size(46, 43);
            this.buttonStop.TabIndex = 0;
            this.buttonStop.UseVisualStyleBackColor = false;
            this.buttonStop.Click += new System.EventHandler(this.buttonStop_Click);
            // 
            // pictureBoxLogoAnax
            // 
            this.pictureBoxLogoAnax.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBoxLogoAnax.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxLogoAnax.Image = global::Heraclitus.Properties.Resources.anaxgroup_logo;
            this.pictureBoxLogoAnax.Location = new System.Drawing.Point(3, 3);
            this.pictureBoxLogoAnax.Name = "pictureBoxLogoAnax";
            this.pictureBoxLogoAnax.Size = new System.Drawing.Size(245, 43);
            this.pictureBoxLogoAnax.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxLogoAnax.TabIndex = 4;
            this.pictureBoxLogoAnax.TabStop = false;
            this.pictureBoxLogoAnax.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBoxLogoAnax_MouseClick);
            // 
            // pictureBoxLogoNop
            // 
            this.pictureBoxLogoNop.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBoxLogoNop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxLogoNop.Image = global::Heraclitus.Properties.Resources.nopservices_logo;
            this.pictureBoxLogoNop.Location = new System.Drawing.Point(410, 3);
            this.pictureBoxLogoNop.Name = "pictureBoxLogoNop";
            this.pictureBoxLogoNop.Size = new System.Drawing.Size(245, 43);
            this.pictureBoxLogoNop.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxLogoNop.TabIndex = 5;
            this.pictureBoxLogoNop.TabStop = false;
            this.pictureBoxLogoNop.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBoxLogoNop_MouseClick);
            // 
            // trackBar1
            // 
            this.trackBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.trackBar1.Location = new System.Drawing.Point(0, 0);
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(658, 45);
            this.trackBar1.TabIndex = 1;
            this.trackBar1.TabStop = false;
            this.trackBar1.ValueChanged += new System.EventHandler(this.trackBar1_ValueChanged);
            this.trackBar1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.trackBar1_MouseDown);
            this.trackBar1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.trackBar1_MouseUp);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.buttonSetColorTractor);
            this.groupBox4.Controls.Add(this.buttonSetColorRoof);
            this.groupBox4.Controls.Add(this.panelSelectedColor);
            this.groupBox4.Controls.Add(this.checkBoxEnableColorDetection);
            this.groupBox4.Location = new System.Drawing.Point(3, 544);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(10);
            this.groupBox4.Size = new System.Drawing.Size(144, 94);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Color Detection";
            // 
            // buttonSetColorTractor
            // 
            this.buttonSetColorTractor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(234)))), ((int)(((byte)(246)))));
            this.buttonSetColorTractor.FlatAppearance.BorderSize = 0;
            this.buttonSetColorTractor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSetColorTractor.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.buttonSetColorTractor.Location = new System.Drawing.Point(76, 62);
            this.buttonSetColorTractor.Name = "buttonSetColorTractor";
            this.buttonSetColorTractor.Size = new System.Drawing.Size(58, 23);
            this.buttonSetColorTractor.TabIndex = 3;
            this.buttonSetColorTractor.Text = "Tractor";
            this.buttonSetColorTractor.UseVisualStyleBackColor = false;
            this.buttonSetColorTractor.Click += new System.EventHandler(this.buttonSetColorTractor_Click);
            // 
            // buttonSetColorRoof
            // 
            this.buttonSetColorRoof.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(175)))), ((int)(((byte)(177)))));
            this.buttonSetColorRoof.FlatAppearance.BorderSize = 0;
            this.buttonSetColorRoof.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSetColorRoof.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.buttonSetColorRoof.Location = new System.Drawing.Point(76, 41);
            this.buttonSetColorRoof.Name = "buttonSetColorRoof";
            this.buttonSetColorRoof.Size = new System.Drawing.Size(58, 20);
            this.buttonSetColorRoof.TabIndex = 0;
            this.buttonSetColorRoof.Text = "Roof";
            this.buttonSetColorRoof.UseVisualStyleBackColor = false;
            this.buttonSetColorRoof.Click += new System.EventHandler(this.buttonSetColorRoof_Click);
            // 
            // panelSelectedColor
            // 
            this.panelSelectedColor.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelSelectedColor.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelSelectedColor.Location = new System.Drawing.Point(10, 41);
            this.panelSelectedColor.Name = "panelSelectedColor";
            this.panelSelectedColor.Size = new System.Drawing.Size(60, 43);
            this.panelSelectedColor.TabIndex = 2;
            this.panelSelectedColor.Click += new System.EventHandler(this.panelSelectedColor_Click);
            // 
            // checkBoxEnableColorDetection
            // 
            this.checkBoxEnableColorDetection.Dock = System.Windows.Forms.DockStyle.Top;
            this.checkBoxEnableColorDetection.Location = new System.Drawing.Point(10, 23);
            this.checkBoxEnableColorDetection.Name = "checkBoxEnableColorDetection";
            this.checkBoxEnableColorDetection.Size = new System.Drawing.Size(124, 18);
            this.checkBoxEnableColorDetection.TabIndex = 1;
            this.checkBoxEnableColorDetection.Text = "Enable";
            this.checkBoxEnableColorDetection.UseVisualStyleBackColor = true;
            this.checkBoxEnableColorDetection.CheckedChanged += new System.EventHandler(this.checkBoxEnableColorDetection_CheckedChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuClose,
            this.mnuOpen,
            this.streamingToolStripMenuItem,
            this.cameraToolStripMenuItem,
            this.aboutToolStripMenuItem,
            this.toolStripComboBoxSystemCameras});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1264, 40);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // mnuClose
            // 
            this.mnuClose.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.mnuClose.Image = global::Heraclitus.Properties.Resources.icon_close;
            this.mnuClose.ImageTransparentColor = System.Drawing.Color.Gray;
            this.mnuClose.Name = "mnuClose";
            this.mnuClose.Size = new System.Drawing.Size(44, 36);
            this.mnuClose.Text = "Close";
            this.mnuClose.ToolTipText = "Close";
            this.mnuClose.Visible = false;
            this.mnuClose.Click += new System.EventHandler(this.mnuClose_Click);
            // 
            // mnuOpen
            // 
            this.mnuOpen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.mnuOpen.Image = global::Heraclitus.Properties.Resources.icon_movie_512;
            this.mnuOpen.ImageTransparentColor = System.Drawing.Color.Gray;
            this.mnuOpen.Name = "mnuOpen";
            this.mnuOpen.Size = new System.Drawing.Size(44, 36);
            this.mnuOpen.Text = "Open";
            this.mnuOpen.ToolTipText = "Open Video File";
            this.mnuOpen.Click += new System.EventHandler(this.mnuOpen_Click);
            // 
            // streamingToolStripMenuItem
            // 
            this.streamingToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.streamingToolStripMenuItem.Image = global::Heraclitus.Properties.Resources.streaming_icon;
            this.streamingToolStripMenuItem.Name = "streamingToolStripMenuItem";
            this.streamingToolStripMenuItem.Size = new System.Drawing.Size(44, 36);
            this.streamingToolStripMenuItem.Text = "Camera";
            this.streamingToolStripMenuItem.ToolTipText = "Streaming";
            this.streamingToolStripMenuItem.Click += new System.EventHandler(this.streamingToolStripMenuItem_Click);
            // 
            // cameraToolStripMenuItem
            // 
            this.cameraToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.cameraToolStripMenuItem.Image = global::Heraclitus.Properties.Resources.camera_icon;
            this.cameraToolStripMenuItem.Name = "cameraToolStripMenuItem";
            this.cameraToolStripMenuItem.Size = new System.Drawing.Size(44, 36);
            this.cameraToolStripMenuItem.Text = "Camera";
            this.cameraToolStripMenuItem.ToolTipText = "Camera";
            this.cameraToolStripMenuItem.Click += new System.EventHandler(this.cameraToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 36);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // toolStripComboBoxSystemCameras
            // 
            this.toolStripComboBoxSystemCameras.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolStripComboBoxSystemCameras.MaxDropDownItems = 50;
            this.toolStripComboBoxSystemCameras.Name = "toolStripComboBoxSystemCameras";
            this.toolStripComboBoxSystemCameras.Size = new System.Drawing.Size(200, 36);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "mp4";
            this.openFileDialog1.Filter = "mp4 file|*.mp4|All files|*.*";
            this.openFileDialog1.Title = "Open Video File";
            // 
            // openFileDialogCsvMetadata
            // 
            this.openFileDialogCsvMetadata.DefaultExt = "csv";
            this.openFileDialogCsvMetadata.Filter = "csv file|*.csv|All files|*.*";
            this.openFileDialogCsvMetadata.Title = "Load Flight Data";
            // 
            // colorDetectionDialog
            // 
            this.colorDetectionDialog.AnyColor = true;
            this.colorDetectionDialog.Color = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(175)))), ((int)(((byte)(177)))));
            this.colorDetectionDialog.FullOpen = true;
            this.colorDetectionDialog.SolidColorOnly = true;
            // 
            // printDocument1
            // 
            this.printDocument1.DocumentName = "Heraclitus Map";
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // printDialog1
            // 
            this.printDialog1.Document = this.printDocument1;
            this.printDialog1.UseEXDialog = true;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 681);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(900, 536);
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Heraclitus";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmMain_FormClosed);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numFeaturesDetectorThreshold)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numMotionDetectorErodeFactor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMotionDetectorDilateFactor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMotionDetectorThreshold)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFlightPath)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogoAnax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogoNop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem mnuOpen;
        private System.Windows.Forms.ToolStripMenuItem mnuClose;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.ToolStripMenuItem cameraToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox checkBoxUseBackgroundEstimation;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelFeaturesThreshold;
        private System.Windows.Forms.NumericUpDown numFeaturesDetectorThreshold;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label labelMotionThreshold;
        private System.Windows.Forms.NumericUpDown numMotionDetectorThreshold;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.PictureBox pictureBoxFlightPath;
        private System.Windows.Forms.TrackBar trackBar2;
        private System.Windows.Forms.Label labelFlightDataFile;
        private System.Windows.Forms.Button buttonLoadFlightData;
        private System.Windows.Forms.OpenFileDialog openFileDialogCsvMetadata;
        private System.Windows.Forms.Button buttonClearFlightData;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Button buttonPlay;
        private System.Windows.Forms.Button buttonPause;
        private System.Windows.Forms.Button buttonStop;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckBox checkBoxEnableColorDetection;
        private System.Windows.Forms.ColorDialog colorDetectionDialog;
        private System.Windows.Forms.Panel panelSelectedColor;
        private System.Windows.Forms.PictureBox pictureBoxLogoAnax;
        private System.Windows.Forms.PictureBox pictureBoxLogoNop;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.Button buttonSetColorTractor;
        private System.Windows.Forms.Button buttonSetColorRoof;
        private System.Windows.Forms.ToolStripMenuItem streamingToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBoxSystemCameras;
        private HeraclitusMapLib.Controls.MapSimpleButtonPanel mapSimpleButtonPanel1;
        private HeraclitusMapLib.Controls.MapCtl mapCtl1;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.PrintDialog printDialog1;
        private System.Windows.Forms.Label labelMotionDetectorDilateFactor;
        private System.Windows.Forms.NumericUpDown numMotionDetectorDilateFactor;
        private System.Windows.Forms.Label labelMotionDetectorErodeFactor;
        private System.Windows.Forms.NumericUpDown numMotionDetectorErodeFactor;
    }
}

