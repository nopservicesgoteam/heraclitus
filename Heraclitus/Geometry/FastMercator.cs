﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heraclitus.Geometry
{
    public class FastMercator
    {
        public const double minX = -180;
        public const double maxX = 180;
        public const double minY = -180;
        public const double maxY = 180;

        public static double ClampX(double d)
        {
            return GeometryMath.clamp(d, minX, maxX);
        }
        public static double ClampY(double d)
        {
            return GeometryMath.clamp(d, minY, maxY);
        }

        public static double YToLat(double y)
        {
            return GeometryMath.RadToDeg(2.0 * Math.Atan(Math.Tanh(0.5 * GeometryMath.DegToRad(y))));
        }

        public static double LatToY(double lat)
        {
            double sinx = Math.Sin(GeometryMath.DegToRad(GeometryMath.clamp(lat, -86.0, 86.0)));
            double res = GeometryMath.RadToDeg(0.5 * Math.Log((1.0 + sinx) / (1.0 - sinx)));
            return ClampY(res);
        }

        public static double XToLon(double x)
        {
            return x;
        }

        public static double LonToX(double lon)
        {
            return lon;
        }

    }

}
