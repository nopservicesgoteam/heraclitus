﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heraclitus.Geometry
{
    public class Bounds
    {
        public double minLat = 0;
        public double maxLat = 0;
        public double minLon = 0;
        public double maxLon = 0;

        public bool FoundMinMax = false;
        public void UpdateMinMax(double lat, double lon)
        {
            if (lat > maxLat || !FoundMinMax) maxLat = lat;
            if (lat < minLat || !FoundMinMax) minLat = lat;

            if (lon > maxLon || !FoundMinMax) maxLon = lon;
            if (lon < minLon || !FoundMinMax) minLon = lon;

            FoundMinMax = true;
        }

        public double GetLatRange()
        {
            return maxLat - minLat;
        }

        public double GetLonRange()
        {
            return maxLon - minLon;
        }

    }
}
