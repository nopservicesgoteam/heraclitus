﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heraclitus.Geometry
{
    public class GeometryMath
    {

        public static double RadToDeg(double rad)
        {
            return rad * 180.0 / Math.PI;
        }

        public static double DegToRad(double deg)
        {
            return deg * Math.PI / 180.0;
        }

        public static double clamp(double x, double xmin, double xmax)
        {
            if (x > xmax)
                return xmax;
            if (x < xmin)
                return xmin;
            return x;
        }
    }
}
