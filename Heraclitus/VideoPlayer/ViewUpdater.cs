﻿using Heraclitus.Metadata;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hearclitus.VideoPlayer
{
    public class ViewUpdater
    {
        private Thread updateThread;
        private Thread disposeThread;
        private bool _isRunning;
        private bool _hasFinished;

        private PictureBox _pictureBox;
        private TrackBar _trackBar;
        private ReaderBuffer _readBuffer;
        private FlightMetadata _flightMetadata;

        private AutoResetEvent _waitObject = new AutoResetEvent(false);

        public ViewUpdater(ReaderBuffer readerBuffer, TrackBar trackBar, PictureBox pictureBox, FlightMetadata flightMetadata)
        {
            this._readBuffer = readerBuffer;
            this._trackBar = trackBar;
            this._pictureBox = pictureBox;
            this._flightMetadata = flightMetadata;
            this._isRunning = false;
        }

        public void Start()
        {
            this._isRunning = true;
            this._hasFinished = false;

            this.disposeThread = new Thread(disposeProc);
            this.disposeThread.IsBackground = true;
            this.disposeThread.Priority = ThreadPriority.BelowNormal;
            this.disposeThread.Start();

            this.updateThread = new Thread(updateProc);
            this.updateThread.IsBackground = true;
            this.updateThread.Priority = ThreadPriority.Highest;
            this.updateThread.Start();
        }

        private void disposeUnusedFrames()
        {
            VideoFrame frame;
            while (unusedFrames.Count > 0)
            {
                frame = unusedFrames.Dequeue();
                frame.Dispose();
            }
        }

        private void disposeProc()
        {
            while (this._isRunning)
            {
                disposeUnusedFrames();
                Thread.Sleep(20);
            }
        }

        public bool enableTrackBarUpdate = true;

        Queue<VideoFrame> unusedFrames = new Queue<VideoFrame>();
        VideoFrame currentFrame = null;
        VideoFrame prevFrame = null;
        private void updateImage(VideoFrame frame)
        {
            currentFrame = frame;
            if (this._flightMetadata != null && frame != null) this._flightMetadata.MoveToTimeMilliseconds(frame.Seconds * 1e3);

            if (currentFrame != null && currentFrame.EventMessage != null)
            {
                StringBuilder em = new StringBuilder();
                em.Append(currentFrame.EventMessage);
                if (this._flightMetadata != null)
                {
                    FlightPositionInfo curMeta = this._flightMetadata.GetCurrentInfo();
                    if (curMeta != null)
                    {
                        em.AppendFormat(" at (lat,lon)=({0:0.00000}, {0:0.00000})", curMeta.Latitude, curMeta.Longitude);
                    }                    
                }
                this._readBuffer.LogEvent(em.ToString());
            }


            this._pictureBox.Invoke(new Action(() => {
                //Console.WriteLine("Showing frame #{0}", frame.FrameNumber);
                if (frame != null)
                {
                    this._pictureBox.Image = frame.Frame.Bitmap;

                    if (this.enableTrackBarUpdate)
                    {
                        this._trackBar.Minimum = 0;
                        this._trackBar.Maximum = this._readBuffer.TotalFrames;
                        this._trackBar.Value = (this._readBuffer.FileMode) ? frame.FrameNumber : 0;
                    }

                }
                else
                {
                    this._pictureBox.Image = null;

                    if (this.enableTrackBarUpdate)
                    {
                        //this._trackBar.Minimum = 0;
                        //this._trackBar.Maximum = 100;
                        //this._trackBar.Value = 0;
                    }
                }
                if (prevFrame != null)
                {
                    unusedFrames.Enqueue(prevFrame);            
                }
                prevFrame = frame;
            }));

        }

        private void updateProc()
        {
            int waitMillis = 20;
            VideoFrame curFrame = null;
            VideoFrame pf = null;
            var sw = new Stopwatch();
            double lastUpdateTime = (DateTime.Now.Ticks / 10000.0) - 5e3;
            double curTime;
            bool isBuffering = false;

            while (this._isRunning)
            {
                sw.Reset();
                sw.Start();
                isBuffering = false;

                switch (this._readBuffer.CurrentState)
                {
                    case State.Playing:

                        curFrame = this._readBuffer.GetFrame();
                        if (curFrame != null)
                        {
                            if (pf == null && this._readBuffer._filename != null)
                            {
                                lastUpdateTime -= 5e3;
                            }
                            this.updateImage(curFrame);
                        }
                        if (curFrame == null)
                        {
                            if (this._readBuffer._isReading)
                            {
                                //is buffering
                                isBuffering = true;
                            }else {
                                //has finished
                                this._readBuffer.CurrentState = State.Stopped;
                                this.updateImage(null);
                            }                            
                        }
                        pf = curFrame;
                        break;

                    case State.Stopped:
                        if (this._pictureBox.Image != null)
                        {
                            this.updateImage(null);
                        }
                        break;
                }

                if (this._readBuffer._frameWaitMillis > 0) waitMillis = this._readBuffer._frameWaitMillis;

                curTime = DateTime.Now.Ticks / 10000.0;
                if (curTime - lastUpdateTime >= 1000)
                {
                    showStats();
                    lastUpdateTime = curTime;
                }

                sw.Stop();

                //Console.WriteLine("T={0}", sw.ElapsedMilliseconds);
                
                waitMillis -= (int)sw.ElapsedMilliseconds;
                if (isBuffering) waitMillis = 5;

                if (waitMillis < 0) {
                    waitMillis = 0;
                }
                //Console.WriteLine("W={0}, T={1}", waitMillis, sw.ElapsedMilliseconds);
                _waitObject.WaitOne(waitMillis);                
            }
            this._hasFinished = true;
            Console.WriteLine("Video updater finished");
        }

        private void showStats()
        {
            if (currentFrame != null)
            {
                //Console.WriteLine("#[{0}] @{1} ({2})", currentFrame.FrameNumber, currentFrame.Seconds, this._readBuffer.buffer.Count);
            }            
        }

        public void Stop(bool wait = false)
        {
            if (this._isRunning)
            {
                this._isRunning = false;
                this._waitObject.Set();
                if (wait) {
                    while (!this._hasFinished)
                    {                        
                        Application.DoEvents();
                    }
                    this.updateThread.Join();
                    this.disposeThread.Join();
                }
                this.updateThread = null;
            }
        }

    }
}
