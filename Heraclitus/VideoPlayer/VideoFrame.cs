﻿using Emgu.CV;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hearclitus.VideoPlayer
{
    public class VideoFrame : IDisposable
    {
        public int FrameNumber = 0;
        public double Seconds = 0;
        public Mat Frame;
        public String EventMessage = null;

        public VideoFrame()
        {
            this.Frame = new Mat();
        }

        public VideoFrame(Mat aFrame)
        {
            this.Frame = aFrame;
        }

        public void Dispose()
        {
            //Console.WriteLine("Disposing Video Frame #{0}", this.FrameNumber);
            this.Frame.Dispose();
        }

        public void SetEvent(string eventMessage)
        {
            this.EventMessage = eventMessage;
        }
    }
}
