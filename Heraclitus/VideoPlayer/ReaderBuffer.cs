﻿using Emgu.CV;
using Emgu.CV.BgSegm;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Hearclitus.ImageProcessing;
using Heraclitus.Properties;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hearclitus.VideoPlayer
{
    public enum State
    {
        Stopped,
        Playing,
        Paused
    }

    public class ReaderBuffer
    {
        public Queue<VideoFrame> buffer;
        public State CurrentState = State.Stopped;

        public const int MAX_BUFFER_SIZE = 20; //max no of stored frames
        public const int MIN_BUFFER_SIZE = 10; //max no of stored frames
        public const double EVENT_FRAMES_EVERY_SEC = 1.0;

        private Thread readerThread;
        private VideoCapture _videoCapture = null;        
        public string _filename;
        public bool _isReading;                
        private int _totalFrames;
        private int _curFrameNo;
        private int _FPS;
        public int _frameWaitMillis;
        private bool _hasStartedDeliveringFrames;
        private bool _fileMode = false;
        private bool _cameraMode = false;
        private bool _streamingMode = false;
        private String _eventsDir = null;        
        private bool _prevHasMatches = false;
        private double _lastEventFrameSeconds = -1e9;
        private StreamWriter _eventsLog = null;
        

        private bool _motionDetectorHasBeenSetup = false;
        public MotionDetectorWithBackgroundWarping MotionDetector = new MotionDetectorWithBackgroundWarping();

        private AutoResetEvent _waitObject = new AutoResetEvent(false);

        private Nullable<int> _forceMoveToFrameNo = null;

        public ReaderBuffer()
        {
            this.readerThread = null;
            this.buffer = new Queue<VideoFrame>();
            this._isReading = false;
            this.TotalFrames = 0;
            this._FPS = 0;
            this._frameWaitMillis = 0;
            this.CurrentState = State.Stopped;
            this._hasStartedDeliveringFrames = false;
            this._fileMode = false;
            this._cameraMode = false;
            this._streamingMode = false;
            this._prevHasMatches = false;
            this._lastEventFrameSeconds = -1e9;
            this._eventsLog = null;
        }

        public int TotalFrames { get => _totalFrames; set => _totalFrames = value; }
        public bool FileMode { get => _fileMode; set => _fileMode = value; }

        public bool Start(string filename)
        {
            this.Stop(true);
            this._filename = filename;

            this._prevHasMatches = false;
            this._lastEventFrameSeconds = -1e9;
            this._eventsLog = null;

            if (filename != null || true)
            {
                //setup events folder
                String basedir;
                String clearname;

                this._cameraMode = (filename.StartsWith("videodevice://"));
                if (this._cameraMode)
                {
                    int videoDeviceID = Int32.Parse(filename.Substring(14));
                    this._streamingMode = false;
                    this._fileMode = false;
                    this._videoCapture = new VideoCapture(videoDeviceID);
                }
                else
                {
                    this._streamingMode = this._filename.StartsWith("rtmp://");
                    this._fileMode = !this._streamingMode;

                    this._videoCapture = new VideoCapture(this._filename);
                }

                if (!this._videoCapture.IsOpened)
                {
                    return false;
                }


                if (this._cameraMode)
                {
                    basedir = Settings.Default.DefaultEventsFolder;
                    DateTime now = DateTime.Now;
                    clearname = "camera_" + now.ToString("yyyyMMdd_hhmmss");
                }
                else if (this._streamingMode)
                {
                    basedir = Settings.Default.DefaultEventsFolder;
                    DateTime now = DateTime.Now;
                    clearname = "streaming_" + now.ToString("yyyyMMdd_hhmmss");
                }
                else
                {
                    basedir = Path.GetDirectoryName(this._filename);
                    clearname = Path.GetFileNameWithoutExtension(this._filename);
                }

                _eventsDir = Path.Combine(basedir, clearname);
                try
                {
                    DirectoryInfo edi = Directory.CreateDirectory(_eventsDir);
                    foreach (FileInfo file in edi.GetFiles())
                    {
                        try
                        {
                            file.Delete();
                        }
                        catch (Exception)
                        {

                        }                        
                    }
                    foreach (DirectoryInfo dir in edi.GetDirectories())
                    {
                        try
                        {
                            dir.Delete(true);
                        }
                        catch (Exception)
                        {
                        }
                        
                    }
                }
                catch (Exception)
                {
                    _eventsDir = null;                    
                }
                
                this._eventsLog = new StreamWriter(Path.Combine(this._eventsDir, "eventslog.txt"));
            }
            else
            {
                this._cameraMode = true;
                this._videoCapture = new VideoCapture(0);

                //_videoCapture.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.Fps, 30);
                //_videoCapture.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameHeight, 120);
                //_videoCapture.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameWidth, 160);
                this._FPS = 100;
                this._frameWaitMillis = Convert.ToInt32(1000.0 / this._FPS);

                this._videoCapture.ImageGrabbed += _videoCapture_ImageGrabbed;
                this._videoCapture.Start();
                this._isReading = true;
                this.CurrentState = State.Playing;
                return true;
            }


            

            this._isReading = true;
            this.CurrentState = State.Playing;
            if (this._fileMode)
            {
                this.TotalFrames = Convert.ToInt32(this._videoCapture.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameCount));                
            }
            else
            {
                this.TotalFrames = 0;
            }         
            if (this._cameraMode)
            {
                this._FPS = Settings.Default.CameraFPS;
            }
            else
            {
                this._FPS = Convert.ToInt32(this._videoCapture.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.Fps));
            }
            this._frameWaitMillis = Convert.ToInt32(1000.0 / this._FPS);
            this._curFrameNo = 0;
            this._forceMoveToFrameNo = null;
            this._hasStartedDeliveringFrames = false;

            this._motionDetectorHasBeenSetup = false;            

            this.readerThread = new Thread(readerProc);
            this.readerThread.IsBackground = true;
            this.readerThread.Priority = ThreadPriority.Highest;
            this.readerThread.Start();

            return true;
        }

        public void LogEvent(string eventMessage)
        {
            if (this._eventsLog != null)
            {
                this._eventsLog.WriteLine(eventMessage);
                this._eventsLog.Flush();
            }
        }

        bool retrievingCameraFrame = false;

        private void _videoCapture_ImageGrabbed(object sender, EventArgs e)
        {
            retrievingCameraFrame = true;
            VideoFrame currentFrame = new VideoFrame();
            currentFrame.FrameNumber = this._curFrameNo;

            this._videoCapture.Retrieve(currentFrame.Frame);

            this.buffer.Enqueue(currentFrame);
            retrievingCameraFrame = false;
        }

        private void EmptyBuffer()
        {
            VideoFrame frame;
            while (this.buffer.Count > 0)
            {
                frame = this.buffer.Dequeue();
                frame.Dispose();
            }
            this.MotionDetector.Reset();
            this._hasStartedDeliveringFrames = false;
        }

        private void readerProc()
        {
            long prev_millis = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
            long minTime = -1;
            long maxTime = -1;
            long sumTime = 0;
            double avgTime = 0;
            long totalFramesRead = 0;

            /*
            Point textPoint = new Point(10, 50);
            MCvScalar textColor = new MCvScalar(255, 255, 255);
            */

            while (this._isReading)
            {                
                if (this._fileMode && this._curFrameNo >= this.TotalFrames)
                {
                    this._isReading = false;
                    //this.CurrentState = State.Stopped;
                    break;
                }

                if (this._fileMode && _forceMoveToFrameNo != null)
                {
                    EmptyBuffer();
                    this._curFrameNo = _forceMoveToFrameNo.Value;                   
                    _forceMoveToFrameNo = null;
                    if (this._curFrameNo < 0) this._curFrameNo = 0;
                    if (this._curFrameNo >= this.TotalFrames - 1) this._curFrameNo = this.TotalFrames - 1;
                    this._videoCapture.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.PosFrames, this._curFrameNo);
                }

                if (this.buffer.Count < ReaderBuffer.MAX_BUFFER_SIZE)
                {
                    long now_millis = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
                    long millis_elapsed = now_millis - prev_millis;
                    prev_millis = now_millis;

                    if (millis_elapsed > maxTime || maxTime < 0) maxTime = millis_elapsed;
                    if (millis_elapsed < minTime || minTime < 0) minTime = millis_elapsed;
                    sumTime += millis_elapsed;
                    totalFramesRead += 1;
                    

                    /*
                    if (this._curFrameNo % 10 == 0)
                    {
                        Console.WriteLine("#{0}", this._curFrameNo);
                    }
                    */
                    //Console.WriteLine("Filling buffer with frame #{0} #{1}", this._curFrameNo, millis_elapsed);
                    //fill the buffer with a new frame                    
                    VideoFrame currentFrame = new VideoFrame();
                    currentFrame.FrameNumber = this._curFrameNo;
                    currentFrame.Seconds = currentFrame.FrameNumber * this._frameWaitMillis / 1000.0;
                    this._videoCapture.Read(currentFrame.Frame);

                    /*
                    if (false)
                    {
                        Bitmap bmp = currentFrame.Frame.Bitmap;
                        bmp.Save(string.Format("C:\\Devlopment\\NopServices\\Hearclitus\\examples\\frames\\frame_{0:000}.bmp", currentFrame.FrameNumber+1));
                    }*/

                    //process frame
                    //CvInvoke.PutText(currentFrame.Frame, 
                    //    string.Format("#{0}", currentFrame.FrameNumber), textPoint, 
                    //    Emgu.CV.CvEnum.FontFace.HersheyPlain, 2.5, textColor, 2);
                    //
                    //

                    if (!this._motionDetectorHasBeenSetup) {
                        this._motionDetectorHasBeenSetup = true;
                        this.MotionDetector.Setup(currentFrame.Frame);
                    }

                    this.MotionDetector.Feed(currentFrame.Frame);

                    currentFrame.Frame.Dispose();
                    if (this.MotionDetector.Options.EstimateMovingBackground)
                    {
                        //debug -> watch warped frame here
                        //currentFrame.Frame = (this.MotionDetector.PrevFrame != null) ? this.MotionDetector.WarpedPrevFrame.Clone() : this.MotionDetector.CurFrame;
                        currentFrame.Frame = this.MotionDetector.CurFrame.Clone();
                    }
                    else
                    {
                        currentFrame.Frame = this.MotionDetector.CurFrame.Clone();
                    }

                    bool hasMatches = false;
                    if (this.MotionDetector.HueDetector.Options.EnableHueDetection)
                    {
                        hasMatches = (this.MotionDetector.HueDetector.Matches.Count > 0);
                        this.MotionDetector.HueDetector.DrawMatchedBoxes(currentFrame.Frame);
                    }else
                    {
                        hasMatches = (this.MotionDetector.MotionDetector.Matches.Count > 0);
                        this.MotionDetector.MotionDetector.DrawMatchedBoxes(currentFrame.Frame);
                    }

                    if (hasMatches != this._prevHasMatches)
                    {
                        if (hasMatches)
                        {
                            if (this._eventsLog != null)
                            {
                                TimeSpan ts = TimeSpan.FromSeconds(currentFrame.Seconds);
                                currentFrame.SetEvent(String.Format("[{0}] EVENT DETECTED", ts.ToString(@"hh\:mm\:ss\.fff")));
                                /*
                                this._eventsLog.WriteLine(
                                    String.Format("[{0}] EVENT DETECTED", ts.ToString(@"hh\:mm\:ss\.fff"))
                                    );
                                this._eventsLog.Flush();*/
                            }
                        }
                        else
                        {
                            this._lastEventFrameSeconds = -1e9;
                            //Console.WriteLine("MATCH(ES) DISAPPEARED");
                        }
                    }                   
                    
                    if (hasMatches && (this._eventsDir != null))
                    {
                        if (currentFrame.Seconds - this._lastEventFrameSeconds >= EVENT_FRAMES_EVERY_SEC)
                        {
                            Console.WriteLine(String.Format("EVENT FRAME {0}:{1}", currentFrame.FrameNumber, currentFrame.Seconds));
                            String frameName = String.Format("event_frame_{0:000000}.bmp", currentFrame.FrameNumber);
                            String framePath = Path.Combine(this._eventsDir, frameName);
                            currentFrame.Frame.Save(framePath);
                            this._lastEventFrameSeconds = currentFrame.Seconds;
                        }
                    }

                    this._prevHasMatches = hasMatches;


                    //

                    this.buffer.Enqueue(currentFrame);
                    //currentFrame.Dispose();                    
                    this._curFrameNo++;
                }
                else
                {
                    this._hasStartedDeliveringFrames = true;
                    //Console.WriteLine("Waiting");
                    this._waitObject.WaitOne(1000);
                    //Console.WriteLine("Woke");
                }
            }

            this.MotionDetector.Finish();

            if (this._eventsLog != null)
            {
                this._eventsLog.Close();
                this._eventsLog = null;
            }

            avgTime = sumTime / (double)totalFramesRead;
            Console.WriteLine("finished reading file");
            Console.WriteLine("FPS={0}", this._FPS);
            Console.WriteLine("avgTime={0:0.00}ms, read frame-rate={2:0.00}, totalFramesRead={1}", avgTime, totalFramesRead, 1000.0/avgTime);
        }

        public VideoFrame GetFrame()
        {
            if (this.buffer.Count == 0)
            {
                if (this._isReading)
                {
                    this._hasStartedDeliveringFrames = false;
                }
                return null;
            }

            //if (this._isReading && !this._hasStartedDeliveringFrames) return null;

            VideoFrame curFrame = this.buffer.Dequeue();

            this._waitObject.Set();

            return curFrame;
        }

        public void Stop(bool wait = false)
        {
            if (this._eventsLog != null)
            {
                this._eventsLog.Close();
                this._eventsLog = null;
            }

            if (this._isReading)
            {
                this._isReading = false;
                this._waitObject.Set();
                if (wait && this.readerThread != null) this.readerThread.Join();
                this.readerThread = null;
            }
            if (this._videoCapture != null)
            {
                this._videoCapture.ImageGrabbed -= _videoCapture_ImageGrabbed;
                while (retrievingCameraFrame)
                {
                    Application.DoEvents();
                }                
                this._videoCapture.Dispose();
                this._videoCapture = null;
            }
            this.CurrentState = State.Stopped;
            this.buffer.Clear();
            this.MotionDetector.Finish();
        }

        public void Pause()
        {
            if (this.CurrentState == State.Playing)
            {
                this.CurrentState = State.Paused;
            }
        }
        public void Unpause()
        {
            if (this.CurrentState == State.Paused)
            {
                this.CurrentState = State.Playing;
            }
        }

        public void GoToFrameNumber(int value)
        {
            this._forceMoveToFrameNo = value;
        }
    }
}
